package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditClientProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.request.RegisterClientRequest;
import net.thumbtack.onlineshop.dto.response.GetInfoAboutClientsResponse;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationClientTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    private void registerClient(String login) {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный",
                "Центральный", login, "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
    }

    @Test
    public void testPost() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();
        Assert.assertEquals(registerClientResponse1.getPhone(), "89137624410");
        Assert.assertEquals(registerClientResponse1.getEmail(), "hello@gmail.com");
        assertNotNull(registerClientResponse1);
    }

    @Test
    public void testRegisterClientPhoneWithSymbols() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "8-913-762-44-10"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();
        Assert.assertEquals(registerClientResponse1.getPhone(), "89137624410");
    }

    @Test
    public void testRegisterClientWrongPhone() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир1234", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89234137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_CONTENT_PHONE.toString()));
        }
    }

    @Test
    public void testEditClientProfile() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers1 = registerClientResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        EditClientProfileRequest editClientProfileRequest = new EditClientProfileRequest("Владимир", "Северный", "Центральный", "hello@gmail.com", "MyAdress123", "89137624410", "1233414243421", "44754575754");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<EditClientProfileRequest> entity = new HttpEntity<>(editClientProfileRequest, header);
        ResponseEntity<String> response1 = template.exchange(address + "clients", HttpMethod.PUT, entity, String.class);
        String response1Body = response1.getBody();
        assertTrue(response1Body.contains("Северный"));
    }

    @Test
    public void testEditClientProfileNullPatronymic() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers1 = registerClientResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        EditClientProfileRequest editClientProfileRequest = new EditClientProfileRequest("Владимир", "Северный", null, "hello@gmail.com", "MyAdress123", "89137624410", "1233414243421", "44754575754");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<EditClientProfileRequest> entity = new HttpEntity<>(editClientProfileRequest, header);
        ResponseEntity<RegisterClientResponse> response1 = template.exchange(address + "clients", HttpMethod.PUT, entity, RegisterClientResponse.class);
        Assert.assertEquals(response1.getBody().getPatronymic(), "");
    }

    @Test
    public void testGetClients() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        registerClient("loginOfUser1");
        registerClient("loginOfUser2");
        registerClient("loginOfUser3");
        registerClient("loginOfUser4");
        HttpEntity<?> entity = new HttpEntity<>(null, header);
        ResponseEntity<GetInfoAboutClientsResponse[]> response1 = template.exchange(address + "clients", HttpMethod.GET, entity, GetInfoAboutClientsResponse[].class);
        Assert.assertEquals(response1.getBody().length, 4);
    }

    @Test
    public void testPostSameName() {
        try {
            HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
            template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
            template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_REGISTER_TRY.toString()));
        }
    }

    @Test
    public void testRegisterClientWrongFirstName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир1234", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterClientWrongLastName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный@!#", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterClientWrongPatronymicName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный%#%@", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PATRONYMICNAME_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterClientNullFirstName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest(null, "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME.toString()));
        }
    }

    @Test
    public void testRegisterClientNullLastName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", null, "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterClientNullLogin() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", null, "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LOGIN.toString()));
        }
    }

    @Test
    public void testRegisterClientNullPassword() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", null, "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PASSWORD.toString()));
        }
    }

    @Test
    public void testRegisterClientNullEmail() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", null, "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_EMAIL.toString()));
        }
    }

    @Test
    public void testRegisterClientNullAddress() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "b", null, "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_ADDRESS.toString()));
        }
    }

    @Test
    public void testRegisterClientNullPhone() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "c", "MyAdress123", null), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PHONE.toString()));
        }
    }

    @Test
    public void testRegisterClientBlankFirstName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME.toString()));
        }
    }

    @Test
    public void testRegisterClientBlankLastName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterClientBlankLogin() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LOGIN.toString()));
        }
    }

    @Test
    public void testRegisterClientBlankPassword() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "", "hello@gmail.com", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PASSWORD.toString()));
        }
    }

    @Test
    public void testRegisterClientBlankEmail() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_EMAIL.toString()));
        }
    }

    @Test
    public void testRegisterClientBlankAddress() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "b", "", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_ADDRESS.toString()));
        }
    }

    @Test
    public void testRegisterClientBlankPhone() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "c", "MyAdress123", ""), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PHONE.toString()));
        }
    }

    @Test
    public void testRegisterClientNullFirstAndLastName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest(null, null, "Центральный", "loginUser12", "1233414243421", "c", "MyAdress123", "3256789667"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME.toString()));
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterClientWithSymbolsInLogin() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12*/@", "1233414243421", "c", "MyAdress123", ""), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LOGIN_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterMinLengthPassword() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1", null, "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MIN_LENGTH_PASSWORD.toString()));
        }
    }

    @Test
    public void testRegisterMaxLengthFirstName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaир", "Северный", "Центральный", "loginUser12", "1", null, "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_FIRSTNAME.toString()));
        }
    }

    @Test
    public void testRegisterMaxLengthLastName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaный", "Центральный", "loginUser12", "1", null, "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterMaxLengtPatronymicName() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Централaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaьный", "loginUser12", "1", null, "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_PATRONYMICNAME.toString()));
        }
    }

    @Test
    public void testRegisterMaxLengthEmail() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "MyAdress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_EMAIL.toString()));
        }
    }

    @Test
    public void testRegisterMaxLengthAddress() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1", null, "MyAdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaress123", "89137624410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_ADDRESS.toString()));
        }
    }

    @Test
    public void testRegisterMaxLengthPhone() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1", null, "MyAdress123", "891376aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa24410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_PHONE.toString()));
        }
    }


    @Test
    public void testRegisterMaxLengthLogin() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaUser12", "1", null, "MyAdress123", "891376aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa24410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_LOGIN.toString()));
        }
    }

    @Test
    public void testRegisterMaxLengthPassword() {
        try {
            template.postForObject(address + "clients",
                    new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", null, "MyAdress123", "891376aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa24410"), RegisterClientResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_PASSWORD.toString()));
        }
    }
}