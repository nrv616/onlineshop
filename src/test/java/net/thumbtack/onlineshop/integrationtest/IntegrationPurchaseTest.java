package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.*;
import net.thumbtack.onlineshop.dto.request.*;
import net.thumbtack.onlineshop.dto.response.*;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.PurchaseService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationPurchaseTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private PurchaseDaoImpl purchaseDao = new PurchaseDaoImpl();
    private PurchaseService purchaseService = new PurchaseService(purchaseDao, new BasketDaoImpl(), new ClientDaoImpl(), new AdminDaoImpl());
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    private String registerClient(String login) {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", login, "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        return headers.getFirst(HttpHeaders.SET_COOKIE);
    }

    private int addProduct(String name, int price, int count, String cookie) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest(name, price, count), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        return addProductResponse.getBody().getId();
    }

    public void addProductToBasket(String cookie, int id1, String name, int count) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, name, 1000, count), header);
        template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
    }

    @Test
    public void testPurchase() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);
        Assert.assertEquals(10000, putDepositResponse.getBody().getDeposit());

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        ResponseEntity<PurchaseResponse> purDepositResponse = template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);
        Assert.assertEquals(95, purDepositResponse.getBody().getCount());
        Assert.assertEquals(purDepositResponse.getBody().getName(), "product1");
    }

   /* @Test
    public void testPurchaseThreads() throws ServerException {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);


        String cookieSecond = registerClient("Client321");
        HttpHeaders headerSecond = new HttpHeaders();
        headerSecond.add("Cookie", cookieSecond);
        headerSecond.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> requestSecond = new HttpEntity<>(new PutDepositRequest(100000), headerSecond);
        ResponseEntity<RegisterClientResponse> putDepositResponseSecond = template.exchange(address + "deposits", HttpMethod.PUT, requestSecond, RegisterClientResponse.class);

        try {
            Runnable runnableFirst = () -> {
                HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 1), header);
                ResponseEntity<PurchaseResponse> purDepositResponse = template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);
            };
            Runnable runnableSecond = () -> {

                HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 1), headerSecond);
                ResponseEntity<PurchaseResponse> purDepositResponse = template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);
            };
            for (int i = 0; i < 50; i++) {
                new Thread(runnableFirst).start();
                new Thread(runnableSecond).start();
            }
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PURSCHASE_TRY.toString()));
        }
    }*/

    @Test
    public void testPurchaseThreads1() throws ServerException {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);


        String cookieSecond = registerClient("Client321");
        HttpHeaders headerSecond = new HttpHeaders();
        headerSecond.add("Cookie", cookieSecond);
        headerSecond.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> requestSecond = new HttpEntity<>(new PutDepositRequest(100000), headerSecond);
        ResponseEntity<RegisterClientResponse> putDepositResponseSecond = template.exchange(address + "deposits", HttpMethod.PUT, requestSecond, RegisterClientResponse.class);


        Runnable runnableFirst = () -> {
            try {
                purchaseService.purchase(cookie.split("=")[1], new PurchaseRequest(id1, "product1", 1000, 100));
            } catch (ServerException e) {
                Assert.assertEquals(ServerErrorCode.INVALID_PURSCHASE_TRY, e.getServerErrorCode());
            }
        };
        Runnable runnableSecond = () -> {
            try {
                purchaseService.purchase(cookieSecond.split("=")[1], new PurchaseRequest(id1, "product1", 1000, 100));
            } catch (ServerException e) {
                Assert.assertEquals(ServerErrorCode.INVALID_PURSCHASE_TRY, e.getServerErrorCode());
            }
        };
        for (int i = 0; i < 50; i++) {
            new Thread(runnableFirst).start();
            new Thread(runnableSecond).start();
        }

    }

    @Test
    public void testGetPurchasesOfClientNullClient() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId(), HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 2);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id1);
        Assert.assertEquals(getResponse.getBody()[1].getId(), id2);
    }

    @Test
    public void testGetPurchasesOfClientNullClientLimitOffsetNull() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId() + "/?limit=1", HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 1);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id1);
    }

    @Test
    public void testGetPurchasesOfClientNullClientLimitOffset() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId() + "/?limit=1&offset=1", HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 1);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id2);
    }

    @Test
    public void testGetPurchasesOfClientNotNullClient() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId() + "/?client=" + registerClientResponse2.getBody().getId(), HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 3);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id1);
        Assert.assertEquals(getResponse.getBody()[1].getId(), id2);
        Assert.assertEquals(getResponse.getBody()[2].getId(), id2);
    }

    @Test
    public void testGetPurchasesOfClientNotNullClientLimitOffsetNull() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);


        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId() + "/?limit=2&client=" + registerClientResponse2.getBody().getId(), HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 2);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id1);
        Assert.assertEquals(getResponse.getBody()[1].getId(), id2);
    }

    @Test
    public void testGetPurchasesOfClientNotNullClientLimitOffset() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId() + "/?limit=1&offset=1&client=" + registerClientResponse2.getBody().getId(), HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 1);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id2);
    }

    @Test
    public void testGetPurchasesOfClientOrderPrice() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 100, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 100, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId() + "/?order=price", HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 2);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id1);
        Assert.assertEquals(getResponse.getBody()[1].getId(), id2);
    }

    @Test
    public void testGetPurchasesOfClientNotNullClientOrderPrice() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 100, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 100, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetPurchasesResponse[]> getResponse = template.exchange(address + "purchases/clients/" + registerClientResponse1.getId() + "/?order=price&client=" + registerClientResponse2.getBody().getId(), HttpMethod.GET, requestGet, GetPurchasesResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 3);
        Assert.assertEquals(getResponse.getBody()[0].getId(), id1);
        Assert.assertEquals(getResponse.getBody()[1].getId(), id2);
        Assert.assertEquals(getResponse.getBody()[2].getId(), id2);
    }

    @Test
    public void testGetClientsBuyingProduct() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetClientsBuyingProductResponse[]> getResponse = template.exchange(address + "purchases/products/" + id1, HttpMethod.GET, requestGet, GetClientsBuyingProductResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 1);
        Assert.assertEquals(getResponse.getBody()[0].getId(), registerClientResponse1.getId());
    }

    @Test
    public void testGetClientsBuyingProductLimitOffsetNull() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetClientsBuyingProductResponse[]> getResponse = template.exchange(address + "purchases/products/" + id1 + "/?limit=0", HttpMethod.GET, requestGet, GetClientsBuyingProductResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 0);
    }

    @Test
    public void testGetClientsBuyingProductLimitOffset() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Северный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);


        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        ResponseEntity<PurchaseResponse> purDepositResponse = template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetClientsBuyingProductResponse[]> getResponse = template.exchange(address + "purchases/products/" + id1 + "/?limit=1&offset=0", HttpMethod.GET, requestGet, GetClientsBuyingProductResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 1);
        Assert.assertEquals(getResponse.getBody()[0].getId(), registerClientResponse1.getId());
    }

    @Test
    public void testGetClientsBuyingProductNotNullProduct() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Теверный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request6 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request6, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetClientsBuyingProductResponse[]> getResponse = template.exchange(address + "purchases/products/" + id1 + "?product=" + id2, HttpMethod.GET, requestGet, GetClientsBuyingProductResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 2);
        Assert.assertEquals(getResponse.getBody()[0].getId(), registerClientResponse1.getId());
        Assert.assertEquals(getResponse.getBody()[1].getId(), registerClientResponse2.getBody().getId());
    }

    @Test
    public void testGetClientsBuyingProductNotNullProductLimitOffsetNull() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        RegisterClientResponse registerClientResponse1 = registerClientResponse.getBody();

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Теверный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetClientsBuyingProductResponse[]> getResponse = template.exchange(address + "purchases/products/" + id1 + "?limit=1&product=" + id2, HttpMethod.GET, requestGet, GetClientsBuyingProductResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 1);
        Assert.assertEquals(getResponse.getBody()[0].getId(), registerClientResponse1.getId());
    }

    @Test
    public void testGetClientsBuyingProductNotNullProductLimitOffset() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "asfdasfasfsafas", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);

        HttpEntity<RegisterClientRequest> request2 = new HttpEntity<>(new RegisterClientRequest("Михаил", "Теверный", "Центральный", "asfzxcvzxcbttrtr", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse2 = template.exchange(address + "clients", HttpMethod.POST, request2, RegisterClientResponse.class);
        String cookie1 = registerClientResponse2.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders headerAdmin = new HttpHeaders();
        headerAdmin.add("Cookie", cookieAdmin);
        headerAdmin.setContentType(MediaType.APPLICATION_JSON);

        HttpHeaders a1 = new HttpHeaders();
        a1.add("Cookie", cookie1);
        a1.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PutDepositRequest> request3 = new HttpEntity<>(new PutDepositRequest(10000), a1);
        template.exchange(address + "deposits", HttpMethod.PUT, request3, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur1 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 5), header);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur1, PurchaseResponse.class);

        HttpEntity<PurchaseRequest> requestPur2 = new HttpEntity<>(new PurchaseRequest(id2, "product2", 1000, 7), a1);
        template.exchange(address + "purchases", HttpMethod.POST, requestPur2, PurchaseResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(headerAdmin);
        ResponseEntity<GetClientsBuyingProductResponse[]> getResponse = template.exchange(address + "purchases/products/" + id1 + "?limit=1&offset=1&product=" + id2, HttpMethod.GET, requestGet, GetClientsBuyingProductResponse[].class);
        Assert.assertEquals(getResponse.getBody().length, 1);
        Assert.assertEquals(getResponse.getBody()[0].getId(), registerClientResponse2.getBody().getId());
    }

    @Test
    public void testPurchaseInvalidCount() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 10, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 100), header);
        try {
            template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_COUNT_PRODUCT.toString()));
        }
    }

    @Test
    public void testPurchaseInvalidDeposit() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 10, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000), header);
        try {
            template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_DEPOSIT_VALUE.toString()));
        }
    }

    @Test
    public void testPurchaseInvalidNameProduct() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 10, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "prodASuct1", 1000), header);
        try {
            template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testPurchaseInvalidPriceProduct() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 10, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 100), header);
        try {
            template.exchange(address + "purchases", HttpMethod.POST, requestPur, PurchaseResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testPurchaseBasket() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        addProductToBasket(cookie, id1, "product1", 10);
        addProductToBasket(cookie, id2, "product2", 50);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        PurchaseRequest purchaseRequest[] = new PurchaseRequest[2];
        purchaseRequest[0] = new PurchaseRequest(id1, "product1", 1000, 0);
        purchaseRequest[1] = new PurchaseRequest(id2, "product2", 1000, 30);
        HttpEntity<PurchaseRequest[]> requestBuy = new HttpEntity<>(purchaseRequest, header);
        ResponseEntity<PurchaseBasketResponse> basketResponse1 = template.exchange(address + "purchases/baskets", HttpMethod.POST, requestBuy, PurchaseBasketResponse.class);
        Assert.assertEquals(basketResponse1.getBody().getBought().size(), 2);
        Assert.assertEquals(basketResponse1.getBody().getRemaining().size(), 1);
    }

    @Test
    public void testPurchaseBasketBiggerCount() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 0, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        addProductToBasket(cookie, id1, "product1", 10);
        addProductToBasket(cookie, id2, "product2", 50);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        PurchaseRequest purchaseRequest[] = new PurchaseRequest[2];
        purchaseRequest[0] = new PurchaseRequest(id1, "product1", 1000, 10);
        purchaseRequest[1] = new PurchaseRequest(id2, "product2", 1000, 300);
        HttpEntity<PurchaseRequest[]> requestBuy = new HttpEntity<>(purchaseRequest, header);
        try {
            template.exchange(address + "purchases/baskets", HttpMethod.POST, requestBuy, PurchaseBasketResponse.class);
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_COUNT_PRODUCT.toString()));
        }
    }

    @Test
    public void testPurchaseBasketNotTheSameName() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        addProductToBasket(cookie, id1, "product1", 10);
        addProductToBasket(cookie, id2, "product2", 50);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        PurchaseRequest purchaseRequest[] = new PurchaseRequest[2];
        purchaseRequest[0] = new PurchaseRequest(id1, "produ", 1000, 10);
        purchaseRequest[1] = new PurchaseRequest(id2, "product2", 1000, 50);
        HttpEntity<PurchaseRequest[]> requestBuy = new HttpEntity<>(purchaseRequest, header);
        try {
            template.exchange(address + "purchases/baskets", HttpMethod.POST, requestBuy, PurchaseBasketResponse.class);
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testPurchaseBasketNotTheSamePrice() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        addProductToBasket(cookie, id1, "product1", 10);
        addProductToBasket(cookie, id2, "product2", 50);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        PurchaseRequest purchaseRequest[] = new PurchaseRequest[2];
        purchaseRequest[0] = new PurchaseRequest(id1, "product1", 100023, 10);
        purchaseRequest[1] = new PurchaseRequest(id2, "product2", 1000, 50);
        HttpEntity<PurchaseRequest[]> requestBuy = new HttpEntity<>(purchaseRequest, header);
        try {
            template.exchange(address + "purchases/baskets", HttpMethod.POST, requestBuy, PurchaseBasketResponse.class);
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testPurchaseBasketNotEnoughMoneyOnDeposit() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        addProductToBasket(cookie, id1, "product1", 10);
        addProductToBasket(cookie, id2, "product2", 50);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        PurchaseRequest purchaseRequest[] = new PurchaseRequest[2];
        purchaseRequest[0] = new PurchaseRequest(id1, "product1", 1000, 10);
        purchaseRequest[1] = new PurchaseRequest(id2, "product2", 1000, 50);
        HttpEntity<PurchaseRequest[]> requestBuy = new HttpEntity<>(purchaseRequest, header);
        try {
            template.exchange(address + "purchases/baskets", HttpMethod.POST, requestBuy, PurchaseBasketResponse.class);
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_DEPOSIT_VALUE.toString()));
        }
    }

    @Test
    public void testPurchaseBasketInvalidIdProduct() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request8 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request8, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        addProductToBasket(cookie, id1, "product1", 10);
        addProductToBasket(cookie, id2, "product2", 50);

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100000000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        PurchaseRequest purchaseRequest[] = new PurchaseRequest[2];
        purchaseRequest[0] = new PurchaseRequest(0, "product1", 1000, 0);
        purchaseRequest[1] = new PurchaseRequest(id2, "product2", 1000, 30);
        HttpEntity<PurchaseRequest[]> requestBuy = new HttpEntity<>(purchaseRequest, header);
        try {
            template.exchange(address + "purchases/baskets", HttpMethod.POST, requestBuy, PurchaseBasketResponse.class);
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.NONEXISTENT_PRODUCT.toString()));
        }
    }
}
