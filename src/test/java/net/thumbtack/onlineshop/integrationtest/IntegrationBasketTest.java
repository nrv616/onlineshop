package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.*;
import net.thumbtack.onlineshop.dto.response.AddProductResponse;
import net.thumbtack.onlineshop.dto.response.PurchaseResponse;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationBasketTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    private String registerClient(String login) {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", login, "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        String set_cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
        return set_cookie;
    }

    private int addProduct(String name, int price, int count, String cookie) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest(name, price, count), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        return addProductResponse.getBody().getId();
    }

    public void addProductToBasket(String cookie, int id1, String name) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, name, 1000, 500000), header);
        template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
    }

    @Test
    public void testAddProductBiggerCountToBasket() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);
        Assert.assertEquals(10000, putDepositResponse.getBody().getDeposit());

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 500000), header);
        ResponseEntity<PurchaseResponse[]> basketResponse = template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
        Assert.assertEquals(500000, basketResponse.getBody()[0].getCount());
    }

    @Test
    public void testAddProductNotEnoughMoneyToBasket() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 500000), header);
        ResponseEntity<PurchaseResponse[]> basketResponse = template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
        Assert.assertEquals(500000, basketResponse.getBody()[0].getCount());
    }

    @Test
    public void testAddProductInvalidNameToBasket() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product150", 1000, 500000), header);
        try {
            template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testAddProductInvalidPriceToBasket() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 2000, 500000), header);
        try {
            template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testBasketDeleteProduct() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);
        Assert.assertEquals(10000, putDepositResponse.getBody().getDeposit());

        HttpEntity<?> requestDel = new HttpEntity<>(header);
        ResponseEntity<String> basketResponse = template.exchange(address + "baskets/" + id1, HttpMethod.DELETE, requestDel, String.class);
        Assert.assertEquals(basketResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testBasketEditProduct() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);
        Assert.assertEquals(10000, putDepositResponse.getBody().getDeposit());

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 500000), header);
        ResponseEntity<PurchaseResponse[]> basketResponse = template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
        Assert.assertEquals(500000, basketResponse.getBody()[0].getCount());

        HttpEntity<PurchaseRequest> requestEdit = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 600000), header);
        ResponseEntity<PurchaseResponse[]> basketResponse1 = template.exchange(address + "baskets", HttpMethod.PUT, requestEdit, PurchaseResponse[].class);
        Assert.assertEquals(600000, basketResponse1.getBody()[0].getCount());
    }

    @Test
    public void testBasketEditProductInvalidName() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);
        Assert.assertEquals(10000, putDepositResponse.getBody().getDeposit());

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 500000), header);
        ResponseEntity<PurchaseResponse[]> basketResponse = template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
        Assert.assertEquals(500000, basketResponse.getBody()[0].getCount());

        HttpEntity<PurchaseRequest> requestEdit = new HttpEntity<>(new PurchaseRequest(id1, "product5", 1000, 600000), header);
        try {
            ResponseEntity<PurchaseResponse[]> basketResponse1 = template.exchange(address + "baskets", HttpMethod.PUT, requestEdit, PurchaseResponse[].class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testBasketEditProductInvalidPrice() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(10000), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);
        Assert.assertEquals(10000, putDepositResponse.getBody().getDeposit());

        HttpEntity<PurchaseRequest> requestPur = new HttpEntity<>(new PurchaseRequest(id1, "product1", 1000, 500000), header);
        ResponseEntity<PurchaseResponse[]> basketResponse = template.exchange(address + "baskets", HttpMethod.POST, requestPur, PurchaseResponse[].class);
        Assert.assertEquals(500000, basketResponse.getBody()[0].getCount());

        HttpEntity<PurchaseRequest> requestEdit = new HttpEntity<>(new PurchaseRequest(id1, "product1", 3000, 600000), header);
        try {
            ResponseEntity<PurchaseResponse[]> basketResponse1 = template.exchange(address + "baskets", HttpMethod.PUT, requestEdit, PurchaseResponse[].class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE.toString()));
        }
    }

    @Test
    public void testBasketGetProducts() {
        String cookie = registerClient("Client123");
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookieAdmin = headers1.getFirst(HttpHeaders.SET_COOKIE);
        int id1 = addProduct("product1", 1000, 100, cookieAdmin);
        int id2 = addProduct("product2", 1000, 100, cookieAdmin);
        addProductToBasket(cookie, id1, "product1");
        addProductToBasket(cookie, id2, "product2");

        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<PurchaseResponse[]> basketResponse1 = template.exchange(address + "baskets", HttpMethod.GET, requestGet, PurchaseResponse[].class);
        Assert.assertEquals("product1", basketResponse1.getBody()[0].getName());
        Assert.assertEquals("product2", basketResponse1.getBody()[1].getName());
    }
}
