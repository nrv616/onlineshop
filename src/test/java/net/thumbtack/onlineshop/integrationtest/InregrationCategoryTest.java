package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.AddCategoryRequest;
import net.thumbtack.onlineshop.dto.request.AddProductRequest;
import net.thumbtack.onlineshop.dto.request.EditCategoryRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.response.AddCategoryResponse;
import net.thumbtack.onlineshop.dto.response.AddProductResponse;
import net.thumbtack.onlineshop.dto.response.GetInfoProductResponse;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class InregrationCategoryTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    private int addCategory(String name, String cookie) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddCategoryRequest> request1 = new HttpEntity<>(new AddCategoryRequest(name, 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponse = template.exchange(address + "categories", HttpMethod.POST, request1, AddCategoryResponse.class);
        return addCategoryResponse.getBody().getId();
    }

    private int addProductWithCategory(String name, String cookie, int id1, int id2) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        List<Integer> categories = new ArrayList<>();
        categories.add(id1);
        categories.add(id2);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest(name, 350, 100, categories), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        return addProductResponse.getBody().getId();
    }

    private int addSubCategory(String name, String cookie, int id) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddCategoryRequest> request2 = new HttpEntity<>(new AddCategoryRequest(name, id), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseEntity1 = template.exchange(address + "categories", HttpMethod.POST, request2, AddCategoryResponse.class);
        return addCategoryResponseEntity1.getBody().getId();
    }

    @Test
    public void testAddCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddCategoryRequest> request1 = new HttpEntity<>(new AddCategoryRequest("nameCategory", 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponse = template.exchange(address + "categories", HttpMethod.POST, request1, AddCategoryResponse.class);
        Assert.assertEquals(addCategoryResponse.getBody().getName(), "nameCategory");
        Assert.assertEquals(addCategoryResponse.getBody().getParentId(), 0);
    }

    @Test
    public void testAddSubCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddCategoryRequest> request1 = new HttpEntity<>(new AddCategoryRequest("nameCategory", 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseEntity = template.exchange(address + "categories", HttpMethod.POST, request1, AddCategoryResponse.class);
        AddCategoryResponse addCategoryResponse = addCategoryResponseEntity.getBody();
        HttpEntity<AddCategoryRequest> request2 = new HttpEntity<>(new AddCategoryRequest("nameSubCategory", addCategoryResponse.getId()), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseEntity1 = template.exchange(address + "categories", HttpMethod.POST, request2, AddCategoryResponse.class);
        AddCategoryResponse addCategoryResponse1 = addCategoryResponseEntity1.getBody();
        Assert.assertEquals(addCategoryResponse1.getParentId(), addCategoryResponse.getId());
        Assert.assertEquals(addCategoryResponse1.getName(), "nameSubCategory");
    }

    @Test
    public void testGetCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseGet = template.exchange(address + "categories/" + id, HttpMethod.GET, requestGet, AddCategoryResponse.class);
        Assert.assertEquals(addCategoryResponseGet.getBody().getName(), "nameCategory");
    }

    @Test
    public void testGetSubCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);
        int idSub = addSubCategory("nameSubCategory", cookie, id);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseGet = template.exchange(address + "categories/" + idSub, HttpMethod.GET, requestGet, AddCategoryResponse.class);
        Assert.assertEquals(addCategoryResponseGet.getBody().getName(), "nameSubCategory");
    }

    @Test
    public void testEditNameCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);

        HttpEntity<EditCategoryRequest> requestUpdate = new HttpEntity<>(new EditCategoryRequest("nameCategoryNew", 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseUpdate = template.exchange(address + "categories/" + id, HttpMethod.PUT, requestUpdate, AddCategoryResponse.class);
        Assert.assertEquals(addCategoryResponseUpdate.getBody().getName(), "nameCategoryNew");
    }

    @Test
    public void testEditNameCategoryNullName() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);

        HttpEntity<EditCategoryRequest> requestUpdate = new HttpEntity<>(new EditCategoryRequest(null, 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseUpdate = template.exchange(address + "categories/" + id, HttpMethod.PUT, requestUpdate, AddCategoryResponse.class);
        Assert.assertEquals(addCategoryResponseUpdate.getBody().getName(), "nameCategory");
    }

    @Test
    public void testEditNameCategoryBlankName() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);

        HttpEntity<EditCategoryRequest> requestUpdate = new HttpEntity<>(new EditCategoryRequest("", 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseUpdate = template.exchange(address + "categories/" + id, HttpMethod.PUT, requestUpdate, AddCategoryResponse.class);
        Assert.assertEquals("nameCategory", addCategoryResponseUpdate.getBody().getName());
    }

    @Test
    public void testEditCategoryNullParent() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);

        HttpEntity<EditCategoryRequest> requestUpdate = new HttpEntity<>(new EditCategoryRequest(null), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseUpdate = template.exchange(address + "categories/" + id, HttpMethod.PUT, requestUpdate, AddCategoryResponse.class);
        Assert.assertEquals("nameCategory", addCategoryResponseUpdate.getBody().getName());
    }

    @Test
    public void testEditCategoryToSubCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);

        HttpEntity<EditCategoryRequest> requestUpdate = new HttpEntity<>(new EditCategoryRequest(null, 2), header);
        try {
            template.exchange(address + "categories/" + id, HttpMethod.PUT, requestUpdate, AddCategoryResponse.class);
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_CATEGORY_EDIT.toString()));
        }
    }

    @Test
    public void testEditNameAndParentOfSubCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);
        int id1 = addCategory("nameCategory1", cookie);
        int idSub = addSubCategory("nameSubCategory", cookie, id);
        HttpEntity<EditCategoryRequest> requestUpdate = new HttpEntity<>(new EditCategoryRequest("nameCategoryNew", id1), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponseUpdate = template.exchange(address + "categories/" + idSub, HttpMethod.PUT, requestUpdate, AddCategoryResponse.class);
        Assert.assertEquals("nameCategoryNew", addCategoryResponseUpdate.getBody().getName());
        Assert.assertEquals(addCategoryResponseUpdate.getBody().getParentId(), id1);
    }

    @Test
    public void testEditSubCategoryToCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id = addCategory("nameCategory", cookie);
        int idSub = addSubCategory("nameSubCategory", cookie, id);

        HttpEntity<EditCategoryRequest> requestUpdate = new HttpEntity<>(new EditCategoryRequest("nameCategoryNew", 0), header);
        try {
            template.exchange(address + "categories/" + idSub, HttpMethod.PUT, requestUpdate, AddCategoryResponse.class);
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_SUB_CATEGORY_EDIT.toString()));
        }
    }

    @Test
    public void testDeleteCategory() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddCategoryRequest> request1 = new HttpEntity<>(new AddCategoryRequest("nameCategory", 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponse = template.exchange(address + "categories", HttpMethod.POST, request1, AddCategoryResponse.class);
        Assert.assertTrue(addCategoryResponse.getBody().getName().equals("nameCategory"));

        HttpEntity<AddCategoryRequest> request2 = new HttpEntity<>(new AddCategoryRequest("nameCategory1", 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponse1 = template.exchange(address + "categories", HttpMethod.POST, request2, AddCategoryResponse.class);

        int idProduct = addProductWithCategory("product", cookie, addCategoryResponse.getBody().getId(), addCategoryResponse1.getBody().getId());

        HttpEntity<?> requestGet1 = new HttpEntity<>(header);
        ResponseEntity<GetInfoProductResponse> getProductResponse = template.exchange(address + "products/" + idProduct, HttpMethod.GET, requestGet1, GetInfoProductResponse.class);
        Assert.assertTrue(getProductResponse.getBody().getCategories().contains("nameCategory"));

        HttpEntity<?> requestDelete = new HttpEntity<>(header);
        ResponseEntity<String> deleteCategoryResponse = template.exchange(address + "categories/" + addCategoryResponse.getBody().getId(), HttpMethod.DELETE, requestDelete, String.class);
        Assert.assertEquals(deleteCategoryResponse.getStatusCode(), HttpStatus.OK);

        HttpEntity<?> requestGet = new HttpEntity<>(header);
        try {
            template.exchange(address + "categories/" + addCategoryResponse.getBody().getId(), HttpMethod.GET, requestGet, AddCategoryResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.NONEXISTENT_CATEGORY.toString()));
        }
        ResponseEntity<GetInfoProductResponse> getProductResponse1 = template.exchange(address + "products/" + idProduct, HttpMethod.GET, requestGet1, GetInfoProductResponse.class);
        Assert.assertTrue(!getProductResponse1.getBody().getCategories().contains("nameCategory"));
    }

    @Test
    public void testGetCategories() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id1 = addCategory("nameCategory1", cookie);
        int id2 = addCategory("nameCategory2", cookie);
        int id3 = addCategory("AAnameCategory3", cookie);
        addSubCategory("nameSubCategory1", cookie, id1);
        addSubCategory("nameSubCategory2", cookie, id1);
        addSubCategory("nameSubCategory3", cookie, id2);
        addSubCategory("nameSubCategory4", cookie, id3);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<AddCategoryResponse[]> getCategoryResponse = template.exchange(address + "categories", HttpMethod.GET, requestGet, AddCategoryResponse[].class);
        Assert.assertEquals("AAnameCategory3", getCategoryResponse.getBody()[0].getName());
        Assert.assertEquals("nameSubCategory4", getCategoryResponse.getBody()[1].getName());
        Assert.assertEquals("nameCategory1", getCategoryResponse.getBody()[2].getName());
    }

}
