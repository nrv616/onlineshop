package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.PutDepositRequest;
import net.thumbtack.onlineshop.dto.request.RegisterClientRequest;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class InterationDepositTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    private String registerClient(String login) {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", login, "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers = registerClientResponse.getHeaders();
        return headers.getFirst(HttpHeaders.SET_COOKIE);
    }

    @Test
    public void testPutDeposit() {
        String cookie = registerClient("Client123");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100), header);
        ResponseEntity<RegisterClientResponse> putDepositResponse = template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);
        Assert.assertEquals(100, putDepositResponse.getBody().getDeposit());
    }

    @Test
    public void testGetDeposit() {
        String cookie = registerClient("Client123");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PutDepositRequest> request1 = new HttpEntity<>(new PutDepositRequest(100), header);
        template.exchange(address + "deposits", HttpMethod.PUT, request1, RegisterClientResponse.class);

        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<RegisterClientResponse> getDepositResponse = template.exchange(address + "deposits", HttpMethod.GET, requestGet, RegisterClientResponse.class);
        Assert.assertEquals(100, getDepositResponse.getBody().getDeposit());
    }
}
