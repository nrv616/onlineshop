package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditAdminProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationAdminTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void testPost() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        RegisterAdminResponse registerAdminResponse1 = registerAdminResponse.getBody();
        assertNotNull(registerAdminResponse1);
    }

    @Test
    public void testEditAdminProfile() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        EditAdminProfileRequest editAdminProfileRequest = new EditAdminProfileRequest("Владимир", "Южный", "Центральный", "worker", "821123231", "44754575754");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<EditAdminProfileRequest> entity = new HttpEntity<>(editAdminProfileRequest, header);
        ResponseEntity<RegisterAdminResponse> response1 = template.exchange(address + "admins", HttpMethod.PUT, entity, RegisterAdminResponse.class);
        assertEquals(response1.getBody().getFirstName(), "Владимир");
    }

    @Test
    public void testEditAdminProfileNullPatronymic() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        EditAdminProfileRequest editAdminProfileRequest = new EditAdminProfileRequest("Владимир", "Южный", null, "worker", "821123231", "44754575754");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<EditAdminProfileRequest> entity = new HttpEntity<>(editAdminProfileRequest, header);
        ResponseEntity<RegisterAdminResponse> response1 = template.exchange(address + "admins", HttpMethod.PUT, entity, RegisterAdminResponse.class);
        assertEquals(response1.getBody().getPatronymicName(), "");
    }

    @Test
    public void testPostSameName() {
        try {
            HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
            template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
            template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_REGISTER_TRY.toString()));
        }
    }

    @Test
    public void testRegisterAdminWrongFirstName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир***", "Северный", "Центральный", "login12345", "82342342342341", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterAdminMaxLengthLastName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северныйнсановлмывыаопиотывоаопваоыаповаоптваоптваапваопаваопавопвп", "Центральный", "login12345", "82342342342341", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminMaxLengthFirstName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимирfafafafafafafafaffafafafafafaffafafafafaffafafafaf", "Северный", "Центральный", "login12345", "82342342342341", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_FIRSTNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminMaxLengthPatronymicName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северный", "Центральfhfhfhfhfhfhffhfhfhfhfhfhfhfhfhfhfhfhfhfhhfhfhfhfhfhный", "login12345", "82342342342341", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_PATRONYMICNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminMaxLengthLogin() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северный", "Центральный", "login12345fhfhfhfhfhfhffhfhfhfhfhfhfhfhfhfhfhfhfhfhhfhfhfhfhfh", "82342342342341", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_LOGIN.toString()));
        }
    }

    @Test
    public void testRegisterAdminMaxLengthPassword() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северный", "Центральный", "login12345", "8234234234234fhfhfhfhfhfhffhfhfhfhfhfhfhfhfhfhfhfhfhfhhfhfhfhfhfh1", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_PASSWORD.toString()));
        }
    }

    @Test
    public void testRegisterAdminMaxLengthPosition() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северный", "Центральный", "login12345", "82342342342341", "workefhfhfhfhfhfhffhfhfhfhfhfhfhfhfhfhfhfhfhfhhfhfhfhfhfhr"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_POSITION.toString()));
        }
    }

    @Test
    public void testRegisterAdminMinLengthPassword() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северный", "Центральный", "login12345", "82", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MIN_LENGTH_PASSWORD.toString()));
        }
    }

    @Test
    public void testRegisterAdminWrongLastName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северный123", "Центральный", "login12345", "82342342342341", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterAdminWrongPatronymicName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "Северный", "Центральный213///", "login12345", "82342342342341", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PATRONYMICNAME_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterAdminNullFirstName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest(null, "b", "c", "login", "821", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminNullLastName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", null, "c", "login", "821", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminNullLogin() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", "b", "c", null, "821", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LOGIN.toString()));
        }
    }

    @Test
    public void testRegisterAdminNullPassword() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", "b", "c", "login", null, "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PASSWORD.toString()));
        }
    }

    @Test
    public void testRegisterAdminNullPosition() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", "b", "c", "login", "821", null), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_POSITION.toString()));
        }
    }

    @Test
    public void testRegisterAdminBlankFirstName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("", "b", "c", "login", "821", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminBlankLastName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("Владимир", "", "Викторович", "login", "821", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminBlankLogin() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", "b", "c", "", "821", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LOGIN.toString()));
        }
    }

    @Test
    public void testRegisterAdminBlankPassword() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", "b", "c", "login", "", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PASSWORD.toString()));
        }
    }

    @Test
    public void testRegisterAdminBlankPosition() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", "b", "c", "login", "821", ""), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_POSITION.toString()));
        }
    }

    @Test
    public void testRegisterAdminNullFirstAndLastName() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest(null, null, "c", "login", "821", ""), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_FIRSTNAME.toString()));
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LASTNAME.toString()));
        }
    }

    @Test
    public void testRegisterAdminWithSymbolsInLogin() {
        try {
            template.postForObject(address + "admins",
                    new RegisterAdminRequest("header", "b", "c", "login*", "821", "worker"), RegisterAdminResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_LOGIN_CONTENT.toString()));
        }
    }

    @Test
    public void testRegisterAdminWithRussainWordsInLogin() {
        RegisterAdminResponse registerAdminResponse1 = template.postForObject(address + "admins",
                new RegisterAdminRequest("Владимир", "Пупкин", "loginФЫВФЫВФЫА", "82asfasfasf1", "worker"), RegisterAdminResponse.class);
        Assert.assertEquals(registerAdminResponse1.getFirstName(), "Владимир");
    }
}