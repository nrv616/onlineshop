package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationDebugTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void testAddCategory() {
        ResponseEntity<String> registerAdminResponse = template.postForEntity(address + "debug/clear", HttpMethod.POST, String.class);
        Assert.assertEquals(registerAdminResponse.getStatusCode(), HttpStatus.OK);
    }
}
