package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.LoginUserRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.request.RegisterClientRequest;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class InregrationUserTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void testGetInfoAboutAdmin() {
        HttpEntity<RegisterAdminRequest> request1 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request1, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        String[] cookie1 = cookie.split("=");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> entity = new HttpEntity<>(null, header);
        ResponseEntity<String> response1 = template.exchange(address + "accounts", HttpMethod.GET, entity, String.class);
        Assert.assertTrue(response1.getBody().contains("worker"));
        Assert.assertTrue(response1.getBody().contains("Владимир"));
        Assert.assertTrue(response1.getBody().contains("Северный"));
    }

    @Test
    public void testGetInfoAboutClient() {
        HttpEntity<RegisterClientRequest> request = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request, RegisterClientResponse.class);
        HttpHeaders headers1 = registerClientResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> entity = new HttpEntity<>(null, header);
        ResponseEntity<String> response1 = template.exchange(address + "accounts", HttpMethod.GET, entity, String.class);
        Assert.assertTrue(response1.getBody().contains("hello@gmail.com"));
        Assert.assertTrue(response1.getBody().contains("Владимир"));
        Assert.assertTrue(response1.getBody().contains("Северный"));
    }

    @Test
    public void testLoginAndLogoutAdmin() {
        HttpEntity<RegisterAdminRequest> request1 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request1, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        String[] cookie1 = cookie.split("=");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> entity = new HttpEntity<>(null, header);
        template.exchange(address + "sessions", HttpMethod.DELETE, entity, String.class);

        HttpEntity<LoginUserRequest> request = new HttpEntity<>(new LoginUserRequest("loginUser12", "821123231"));
        ResponseEntity<String> response = template.exchange(address + "sessions", HttpMethod.POST, request, String.class);
        String registerAdminResponse1 = response.getBody();
        assertNotNull(registerAdminResponse1);
    }

    @Test
    public void testLoginAndLogoutClient() {
        HttpEntity<RegisterClientRequest> request1 = new HttpEntity<>(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410"));
        ResponseEntity<RegisterClientResponse> registerClientResponse = template.exchange(address + "clients", HttpMethod.POST, request1, RegisterClientResponse.class);
        HttpHeaders headers1 = registerClientResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        String[] cookie1 = cookie.split("=");
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> entity = new HttpEntity<>(null, header);
        template.exchange(address + "sessions", HttpMethod.DELETE, entity, String.class);

        HttpEntity<LoginUserRequest> request = new HttpEntity<>(new LoginUserRequest("loginUser12", "1233414243421"));
        ResponseEntity<String> response = template.exchange(address + "sessions", HttpMethod.POST, request, String.class);
        String loginResponse = response.getBody();
        assertNotNull(loginResponse);
    }

    @Test
    public void testLoginCaseInsensetive() {
        HttpEntity<RegisterAdminRequest> request1 = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request1, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> entity = new HttpEntity<>(null, header);
        template.exchange(address + "sessions", HttpMethod.DELETE, entity, String.class);

        HttpEntity<LoginUserRequest> request = new HttpEntity<>(new LoginUserRequest("LOGINUSER12", "821123231"));
        ResponseEntity<String> response = template.exchange(address + "sessions", HttpMethod.POST, request, String.class);
        String registerAdminResponse1 = response.getBody();
        assertNotNull(registerAdminResponse1);
    }
}
