package net.thumbtack.onlineshop.integrationtest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.AddCategoryRequest;
import net.thumbtack.onlineshop.dto.request.AddProductRequest;
import net.thumbtack.onlineshop.dto.request.EditProductRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.response.AddCategoryResponse;
import net.thumbtack.onlineshop.dto.response.AddProductResponse;
import net.thumbtack.onlineshop.dto.response.GetInfoProductResponse;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IntegrationProductTest {
    private static boolean setUpIsDone = false;
    private RestTemplate template = new RestTemplate();
    private CommonDaoImpl commonDao = new CommonDaoImpl();
    private String address = "http://localhost:8080/api/";

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assert.assertTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    private int addCategory(String name, String cookie) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddCategoryRequest> request1 = new HttpEntity<>(new AddCategoryRequest(name, 0), header);
        ResponseEntity<AddCategoryResponse> addCategoryResponse = template.exchange(address + "categories", HttpMethod.POST, request1, AddCategoryResponse.class);
        return addCategoryResponse.getBody().getId();
    }

    private int addProduct(String name, int price, int count, String cookie) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest(name, price, count), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        return addProductResponse.getBody().getId();
    }

    private int addProductWithCategory(String name, String cookie, int price, int count, int id1, int id2) {
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        List<Integer> categories = new ArrayList<>();
        categories.add(id1);
        categories.add(id2);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest(name, price, count, categories), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        return addProductResponse.getBody().getId();
    }


    @Test
    public void testAddProduct() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());
    }

    @Test
    public void testAddProductNegativePrice() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", -350, 100), header);
        try {
            template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_CONTENT_PRICE.toString()));
        }
    }

    @Test
    public void testAddProductNullName() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest(null, 350, 100), header);
        try {
            template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PRODUCT_NAME.toString()));
        }
    }

    @Test
    public void testAddProductBlankName() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("", 350, 100), header);
        try {
            template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_PRODUCT_NAME.toString()));
        }
    }

    @Test
    public void testAddProductInvalidLengthName() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("hffffffffffffffffdssssssssssssssssssssssssssssssskkkkkkkkkkkkkkkkkkkkkkkkkkkkkkssssss", 350, 100), header);
        try {
            template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_MAX_LENGTH_PRODUCT_NAME.toString()));
        }
    }


    @Test
    public void testAddProductWithCategories() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id1 = addCategory("nameCategory1", cookie);
        int id2 = addCategory("nameCategory2", cookie);
        List<Integer> categories = new ArrayList<>();
        categories.add(id1);
        categories.add(id2);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100, categories), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());
    }

    @Test
    public void testEditProduct() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());

        HttpEntity<EditProductRequest> requestEdit = new HttpEntity<>(new EditProductRequest("nameProductEdit", 1000, 500), header);
        ResponseEntity<AddProductResponse> editProductResponse = template.exchange(address + "products/" + addProductResponse.getBody().getId(), HttpMethod.PUT, requestEdit, AddProductResponse.class);
        Assert.assertEquals(editProductResponse.getBody().getName(), "nameProductEdit");
        Assert.assertEquals(editProductResponse.getBody().getCount(), 500);
        Assert.assertEquals(editProductResponse.getBody().getPrice(), 1000);
    }

    @Test
    public void testEditProductPriceAndCount() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());

        HttpEntity<EditProductRequest> requestEdit = new HttpEntity<>(new EditProductRequest(null, 1000, 500), header);
        ResponseEntity<AddProductResponse> editProductResponse = template.exchange(address + "products/" + addProductResponse.getBody().getId(), HttpMethod.PUT, requestEdit, AddProductResponse.class);
        Assert.assertEquals(editProductResponse.getBody().getName(), "nameProduct");
        Assert.assertEquals(editProductResponse.getBody().getCount(), 500);
        Assert.assertEquals(editProductResponse.getBody().getPrice(), 1000);
    }

    @Test
    public void testEditProductPrice() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());

        HttpEntity<EditProductRequest> requestEdit = new HttpEntity<>(new EditProductRequest(null, 1000, null), header);
        ResponseEntity<AddProductResponse> editProductResponse = template.exchange(address + "products/" + addProductResponse.getBody().getId(), HttpMethod.PUT, requestEdit, AddProductResponse.class);
        Assert.assertEquals(editProductResponse.getBody().getName(), "nameProduct");
        Assert.assertEquals(editProductResponse.getBody().getCount(), 100);
        Assert.assertEquals(editProductResponse.getBody().getPrice(), 1000);
    }

    @Test
    public void testEditProductCount() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());

        HttpEntity<EditProductRequest> requestEdit = new HttpEntity<>(new EditProductRequest(null, null, 103), header);
        ResponseEntity<AddProductResponse> editProductResponse = template.exchange(address + "products/" + addProductResponse.getBody().getId(), HttpMethod.PUT, requestEdit, AddProductResponse.class);
        Assert.assertEquals(editProductResponse.getBody().getName(), "nameProduct");
        Assert.assertEquals(editProductResponse.getBody().getCount(), 103);
        Assert.assertEquals(editProductResponse.getBody().getPrice(), 350);
    }

    @Test
    public void testEditProductCategories() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id1 = addCategory("category1", cookie);
        int id2 = addCategory("category2", cookie);
        List<Integer> categoriesId = new ArrayList<>();
        categoriesId.add(id1);
        categoriesId.add(id2);
        int id = addProductWithCategory("nameProduct", cookie, 350, 100, id1, id2);

        HttpEntity<EditProductRequest> requestEdit = new HttpEntity<>(new EditProductRequest(null, null, null, categoriesId), header);
        ResponseEntity<AddProductResponse> editProductResponse = template.exchange(address + "products/" + id, HttpMethod.PUT, requestEdit, AddProductResponse.class);
        Assert.assertEquals(editProductResponse.getBody().getName(), "nameProduct");
        Assert.assertEquals(editProductResponse.getBody().getCount(), 100);
        Assert.assertEquals(editProductResponse.getBody().getPrice(), 350);
        Assert.assertEquals(editProductResponse.getBody().getCategories(), categoriesId);
    }

    @Test
    public void testDeleteProduct() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());

        HttpEntity<?> requestDelete = new HttpEntity<>(header);
        ResponseEntity<String> deleteProductResponse = template.exchange(address + "products/" + addProductResponse.getBody().getId(), HttpMethod.DELETE, requestDelete, String.class);
        Assert.assertEquals(deleteProductResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testGetInfoProductWithCategories() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id1 = addCategory("nameCategory1", cookie);
        int id2 = addCategory("nameCategory2", cookie);
        List<Integer> categories = new ArrayList<>();
        categories.add(id1);
        categories.add(id2);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100, categories), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertEquals("nameProduct", addProductResponse.getBody().getName());

        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<GetInfoProductResponse> getProductResponse = template.exchange(address + "products/" + addProductResponse.getBody().getId(), HttpMethod.GET, requestGet, GetInfoProductResponse.class);
        Assert.assertEquals(getProductResponse.getBody().getName(), "nameProduct");
        Assert.assertEquals(getProductResponse.getBody().getCount(), 100);
        Assert.assertEquals(getProductResponse.getBody().getPrice(), 350);
    }

    @Test
    public void testGetProductsOrderProductCategoriesNull() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        int id1 = addProduct("product543", 350, 100, cookie);
        int id2 = addProduct("product1", 350, 100, cookie);
        int id3 = addProduct("product0", 350, 100, cookie);
        int id4 = addProduct("product2", 350, 100, cookie);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<GetInfoProductResponse[]> getProductResponse = template.exchange(address + "products?order=product", HttpMethod.GET, requestGet, GetInfoProductResponse[].class);
        Assert.assertEquals(getProductResponse.getBody()[0].getId(), id3);
        Assert.assertEquals(getProductResponse.getBody()[1].getId(), id2);
        Assert.assertEquals(getProductResponse.getBody()[2].getId(), id4);
        Assert.assertEquals(getProductResponse.getBody()[3].getId(), id1);
    }

    @Test
    public void testGetProductsOrderCategoryCategoriesNull() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);

        int id1 = addCategory("category1", cookie);
        int id2 = addCategory("category560", cookie);
        int id3 = addCategory("category324", cookie);
        int idProduct1 = addProductWithCategory("product543", cookie, 350, 100, id1, id2);
        int idProduct2 = addProductWithCategory("product1", cookie, 350, 100, id2, id3);
        int idProduct3 = addProductWithCategory("product0", cookie, 350, 100, id1, id3);
        int idProduct4 = addProductWithCategory("product2", cookie, 350, 100, id1, id2);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<GetInfoProductResponse[]> getProductResponse = template.exchange(address + "products?order=category", HttpMethod.GET, requestGet, GetInfoProductResponse[].class);
        Assert.assertEquals(getProductResponse.getBody()[0].getId(), idProduct3);
        Assert.assertEquals(getProductResponse.getBody()[1].getId(), idProduct4);
        Assert.assertEquals(getProductResponse.getBody()[2].getId(), idProduct1);
        Assert.assertEquals(getProductResponse.getBody()[3].getId(), idProduct3);
        Assert.assertEquals(getProductResponse.getBody()[4].getId(), idProduct2);
        Assert.assertEquals(getProductResponse.getBody()[5].getId(), idProduct2);
        Assert.assertEquals(getProductResponse.getBody()[6].getId(), idProduct4);
        Assert.assertEquals(getProductResponse.getBody()[7].getId(), idProduct1);
        Assert.assertEquals(getProductResponse.getBody().length, 8);
    }

    @Test
    public void testGetProductsOrderCategoryCategoriesBlank() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        int id1 = addCategory("category1", cookie);
        int id2 = addCategory("category560", cookie);
        int id3 = addCategory("category324", cookie);
        addProductWithCategory("product543", cookie, 350, 100, id1, id2);
        addProductWithCategory("product1", cookie, 350, 100, id2, id3);
        addProductWithCategory("product0", cookie, 350, 100, id1, id3);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<GetInfoProductResponse[]> getProductResponse = template.exchange(address + "products?order=category&category=", HttpMethod.GET, requestGet, GetInfoProductResponse[].class);
        Assert.assertEquals(getProductResponse.getBody()[0].getId(), addProductResponse.getBody().getId());
        Assert.assertEquals(getProductResponse.getBody().length, 1);
    }

    @Test
    public void testGetProductsOrderCategoryCategories() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        int id1 = addCategory("category1", cookie);
        int id2 = addCategory("category560", cookie);
        int id3 = addCategory("category324", cookie);
        int idProduct1 = addProductWithCategory("product543", cookie, 350, 100, id1, id2);
        int idProduct2 = addProductWithCategory("product1", cookie, 350, 100, id2, id3);
        int idProduct3 = addProductWithCategory("product0", cookie, 350, 100, id1, id3);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<GetInfoProductResponse[]> getProductResponse = template.exchange(address + "products?order=category&category=" + id1 + "," + id2, HttpMethod.GET, requestGet, GetInfoProductResponse[].class);
        Assert.assertEquals(getProductResponse.getBody()[0].getId(), idProduct3);
        Assert.assertEquals(getProductResponse.getBody()[1].getId(), idProduct1);
        Assert.assertEquals(getProductResponse.getBody()[2].getId(), idProduct2);
        Assert.assertEquals(getProductResponse.getBody()[3].getId(), idProduct1);
        Assert.assertEquals(getProductResponse.getBody().length, 4);
    }

    @Test
    public void testGetProductsOrderProductCategories() {
        HttpEntity<RegisterAdminRequest> request = new HttpEntity<>(new RegisterAdminRequest("Владимир", "Северный", "Центральный", "loginUser12", "821123231", "worker"));
        ResponseEntity<RegisterAdminResponse> registerAdminResponse = template.exchange(address + "admins", HttpMethod.POST, request, RegisterAdminResponse.class);
        HttpHeaders headers1 = registerAdminResponse.getHeaders();
        String cookie = headers1.getFirst(HttpHeaders.SET_COOKIE);
        HttpHeaders header = new HttpHeaders();
        header.add("Cookie", cookie);
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddProductRequest> request1 = new HttpEntity<>(new AddProductRequest("nameProduct", 350, 100), header);
        ResponseEntity<AddProductResponse> addProductResponse = template.exchange(address + "products", HttpMethod.POST, request1, AddProductResponse.class);
        Assert.assertTrue(addProductResponse.getBody().getName().equals("nameProduct"));

        int id1 = addCategory("category1", cookie);
        int id2 = addCategory("category560", cookie);
        int id3 = addCategory("category324", cookie);
        int idProduct1 = addProductWithCategory("product543", cookie, 350, 100, id1, id2);
        int idProduct2 = addProductWithCategory("product1", cookie, 350, 100, id2, id3);
        int idProduct3 = addProductWithCategory("product0", cookie, 350, 100, id1, id3);
        HttpEntity<?> requestGet = new HttpEntity<>(header);
        ResponseEntity<GetInfoProductResponse[]> getProductResponse = template.exchange(address + "products?order=product&category=" + id1 + "," + id2, HttpMethod.GET, requestGet, GetInfoProductResponse[].class);
        Assert.assertEquals(getProductResponse.getBody()[0].getId(), idProduct3);
        Assert.assertEquals(getProductResponse.getBody()[1].getId(), idProduct2);
        Assert.assertEquals(getProductResponse.getBody()[2].getId(), idProduct1);
        Assert.assertEquals(getProductResponse.getBody().length, 3);
    }

}
