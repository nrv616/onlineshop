package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.daoimpl.UserDaoImpl;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.UserService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;


public class UserSeviceTest {
    private static boolean setUpIsDone = false;
    private UserService userService = new UserService(new UserDaoImpl(), new AdminDaoImpl(), new ClientDaoImpl());
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public UserSeviceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void logoutNullCookieTest() throws Exception {
        try {
            userService.logout(null);
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }
}
