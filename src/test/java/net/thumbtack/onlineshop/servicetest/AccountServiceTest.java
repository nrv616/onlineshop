package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.AccountDaoImpl;
import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.AccountService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;

public class AccountServiceTest {
    private static boolean setUpIsDone = false;
    private AccountService accountService = new AccountService(new AccountDaoImpl(), new AdminDaoImpl(), new ClientDaoImpl());

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public AccountServiceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void getUserInfoNullCookieTest() throws Exception {
        try {
            accountService.getUserInfo(null);
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }
}
