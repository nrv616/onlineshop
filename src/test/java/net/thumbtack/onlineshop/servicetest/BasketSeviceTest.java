package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.BasketDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.BasketService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;


public class BasketSeviceTest {
    private static boolean setUpIsDone = false;

    private BasketService basketService = new BasketService(new BasketDaoImpl(), new ClientDaoImpl());
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public BasketSeviceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void addProductToBasketNullCookieTest() throws Exception {
        try {
            basketService.addProductToBasket(null, new PurchaseRequest());
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

    @Test
    public void deleteProductNullCookieTest() throws Exception {
        try {
            basketService.deleteProduct(null, 1);
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

    @Test
    public void editProductFromBasketNullCookieTest() throws Exception {
        try {
            basketService.editProductFromBasket(null, new PurchaseRequest());
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

    @Test
    public void getProductsFromBasketNullCookieTest() throws Exception {
        try {
            basketService.getProductsFromBasket(null);
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }


}
