package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.dto.response.GetSettingsResponse;
import net.thumbtack.onlineshop.services.SettingsService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

public class SettingsServiceTest {
    @Value("${max_name_length}")
    private int maxNameLength;
    @Value("${min_password_length}")
    private int minPasswordLength;
    private SettingsService settingsService = new SettingsService();

    @Test
    public void getSettingsTest() throws Exception {
        GetSettingsResponse getSettingsResponse = settingsService.getSettings("ads");
        Assert.assertEquals(getSettingsResponse.getMaxNameLength(), maxNameLength);
        Assert.assertEquals(getSettingsResponse.getMinPasswordLength(), minPasswordLength);
    }
}
