package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CategoryDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.AddCategoryRequest;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.CategoryService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;


public class CategorySeviceTest {
    private static boolean setUpIsDone = false;

    private CategoryService categoryService = new CategoryService(new CategoryDaoImpl(), new AdminDaoImpl());
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public CategorySeviceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void addCategoryNullCookieTest() throws Exception {
        try {
            categoryService.addCategory(null, new AddCategoryRequest());
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

}
