package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.daoimpl.UserDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditAdminProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.AdminService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;

import static org.junit.Assert.assertNotNull;


public class AdminSeviceTest {
    private static boolean setUpIsDone = false;

    private AdminService adminService = new AdminService(new AdminDaoImpl(), new UserDaoImpl());
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public AdminSeviceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void registerAdminTest() throws Exception {
        RegisterAdminResponse a = adminService.registerAdmin(new RegisterAdminRequest("a", "b", "c", "loginAdmin", "821", "worker"), "as");
        assertNotNull(a);
    }

    @Test
    public void editAdminNullCookieTest() throws Exception {
        try {
            adminService.editAdminProfile(null, new EditAdminProfileRequest());
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

}
