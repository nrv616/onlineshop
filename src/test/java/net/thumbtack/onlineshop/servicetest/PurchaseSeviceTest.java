package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.*;
import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.PurchaseService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;

public class PurchaseSeviceTest {
    private static boolean setUpIsDone = false;

    private PurchaseService purchaseService = new PurchaseService(new PurchaseDaoImpl(), new BasketDaoImpl(), new ClientDaoImpl(), new AdminDaoImpl());
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public PurchaseSeviceTest() {

    }


    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void purchaseNullCookieTest() throws Exception {
        try {
            purchaseService.purchase(null, new PurchaseRequest());
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

    @Test
    public void purchaseBasketNullCookieTest() throws Exception {
        try {
            purchaseService.purchaseBasket(null, new PurchaseRequest[3]);
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

}
