package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.SettingsService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;


public class SettingsSeviceTest {
    private static boolean setUpIsDone = false;

    private SettingsService settingsService = new SettingsService();
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public SettingsSeviceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void getSettingsAdminNullCookieTest() throws Exception {
        try {
            settingsService.getSettings(null);
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }
}
