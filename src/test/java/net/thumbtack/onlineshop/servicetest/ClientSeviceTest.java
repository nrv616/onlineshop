package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditClientProfileRequest;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.ClientService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;

public class ClientSeviceTest {
    private static boolean setUpIsDone = false;

    private ClientService clientService = new ClientService(new ClientDaoImpl(), new AdminDaoImpl());
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public ClientSeviceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void editClientNullCookieTest() throws Exception {
        try {
            clientService.editClientProfile(null, new EditClientProfileRequest());
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }
}
