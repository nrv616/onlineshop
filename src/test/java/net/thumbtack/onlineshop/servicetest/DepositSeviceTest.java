package net.thumbtack.onlineshop.servicetest;

import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.daoimpl.DepositDaoImpl;
import net.thumbtack.onlineshop.dto.request.PutDepositRequest;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.DepositService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.*;

public class DepositSeviceTest {
    private static boolean setUpIsDone = false;

    private DepositService depositService = new DepositService(new DepositDaoImpl(), new ClientDaoImpl());
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    public DepositSeviceTest() {

    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Test
    public void putDepositNullCookieTest() throws Exception {
        try {
            depositService.putDeposit(null, new PutDepositRequest());
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

    @Test
    public void getDepositNullCookieTest() throws Exception {
        try {
            depositService.getDeposit(null);
        } catch (ServerException ex) {
            Assert.assertEquals(ex.getServerErrorCode(), ServerErrorCode.INVALID_JAVASESSIONID);
        }
    }

}
