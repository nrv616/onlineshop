package net.thumbtack.onlineshop.endpointtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditClientProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterClientRequest;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.rest.ClientEndPoint;
import net.thumbtack.onlineshop.services.ClientService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ClientEndPoint.class)
public class ClientEndPointTest {
    private static boolean setUpIsDone = false;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private ClientService clientService;

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    @Autowired
    public ClientEndPointTest() {

    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void registerClientTest() throws Exception {
        this.mvc.perform(post("/api/clients").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new RegisterClientRequest("Владимир", "Северный", "Центральный", "loginUser12", "1233414243421", "hello@gmail.com", "MyAdress123", "89137624410")))).andExpect(status().isOk());
    }

    @Test
    public void getInfoAboutClientsTest() throws Exception {
        this.mvc.perform(get("/api/clients").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void editClientProfileTest() throws Exception {
        this.mvc.perform(put("/api/clients").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new EditClientProfileRequest("Владимир", "Северный", "Центральный", "hello@gmail.com", "MyAdress123", "89137624410", "89137624410", "89137624410")))).andExpect(status().isOk());
    }
}
