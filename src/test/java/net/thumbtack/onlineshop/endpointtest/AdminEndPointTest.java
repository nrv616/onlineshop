package net.thumbtack.onlineshop.endpointtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditAdminProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.rest.AdminEndPoint;
import net.thumbtack.onlineshop.services.AdminService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = AdminEndPoint.class)
public class AdminEndPointTest {
    private static boolean setUpIsDone = false;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private AdminService adminService;

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    @Autowired
    public AdminEndPointTest() {

    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void registerAdminTest() throws Exception {
        this.mvc.perform(post("/api/admins").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new RegisterAdminRequest("Михаил", "Пупкин", "Валерьевич", "login1234", "821123231", "worker")))).andExpect(status().isOk());
    }

    @Test
    public void editAdminProfileTest() throws Exception {
        try {
            this.mvc.perform(put("/api/admins").contentType(MediaType.APPLICATION_JSON)
                    .content(new Gson().toJson(new EditAdminProfileRequest("Михаил", "Пупкин", "Валерьевич", "position", "821123231", "32534266334")))).andExpect(status().isOk());
        } catch (HttpClientErrorException exc) {
            assertEquals(400, exc.getStatusCode().value());
            assertTrue(exc.getResponseBodyAsString().contains(ServerErrorCode.INVALID_JAVASESSIONID.toString()));
        }
    }
}
