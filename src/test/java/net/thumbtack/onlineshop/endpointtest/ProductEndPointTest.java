package net.thumbtack.onlineshop.endpointtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.AddProductRequest;
import net.thumbtack.onlineshop.dto.request.EditProductRequest;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.rest.ProductEndPoint;
import net.thumbtack.onlineshop.services.ProductService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProductEndPoint.class)
public class ProductEndPointTest {
    private static boolean setUpIsDone = false;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private ProductService productService;

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    @Autowired
    public ProductEndPointTest() {

    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void addProductTest() throws Exception {
        this.mvc.perform(post("/api/products").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new AddProductRequest("nameProduct", 350, 100)))).andExpect(status().isOk());
    }

    @Test
    public void editProductTest() throws Exception {
        this.mvc.perform(put("/api/products/1").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new EditProductRequest("nameProduct", 350, 100)))).andExpect(status().isOk());
    }

    @Test
    public void deleteProductTest() throws Exception {
        this.mvc.perform(delete("/api/products/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void getInfoProductTest() throws Exception {
        this.mvc.perform(get("/api/products/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void getProductsTest() throws Exception {
        this.mvc.perform(get("/api/products").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
