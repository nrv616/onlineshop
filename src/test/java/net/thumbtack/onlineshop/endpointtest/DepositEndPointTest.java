package net.thumbtack.onlineshop.endpointtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.PutDepositRequest;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.rest.DepositEndPoint;
import net.thumbtack.onlineshop.services.DepositService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = DepositEndPoint.class)
public class DepositEndPointTest {
    private static boolean setUpIsDone = false;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private DepositService depositService;

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    @Autowired
    public DepositEndPointTest() {

    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void putDepositTest() throws Exception {
        this.mvc.perform(put("/api/deposits").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new PutDepositRequest(100)))).andExpect(status().isOk());
    }

    @Test
    public void getDepositTest() throws Exception {
        this.mvc.perform(get("/api/deposits").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
