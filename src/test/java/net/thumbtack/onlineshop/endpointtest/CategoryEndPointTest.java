package net.thumbtack.onlineshop.endpointtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.AddCategoryRequest;
import net.thumbtack.onlineshop.dto.request.EditCategoryRequest;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.rest.CategoryEndPoint;
import net.thumbtack.onlineshop.services.CategoryService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CategoryEndPoint.class)
public class CategoryEndPointTest {
    private static boolean setUpIsDone = false;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private CategoryService categoryService;

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    @Autowired
    public CategoryEndPointTest() {

    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void addCategoryTest() throws Exception {
        this.mvc.perform(post("/api/categories").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new AddCategoryRequest("nameCategory", 0)))).andExpect(status().isOk());
    }

    @Test
    public void getCategoryByIdTest() throws Exception {
        this.mvc.perform(get("/api/categories/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void editCategoryByIdTest() throws Exception {
        this.mvc.perform(put("/api/categories/1").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new EditCategoryRequest("nameCategory", 0)))).andExpect(status().isOk());
    }

    @Test
    public void deleteCategoryByIdTest() throws Exception {
        this.mvc.perform(delete("/api/categories/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void getCategoriesTest() throws Exception {
        this.mvc.perform(get("/api/categories").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
