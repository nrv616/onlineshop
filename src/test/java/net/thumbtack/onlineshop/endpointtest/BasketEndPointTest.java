package net.thumbtack.onlineshop.endpointtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.rest.BasketEndPoint;
import net.thumbtack.onlineshop.services.BasketService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = BasketEndPoint.class)
public class BasketEndPointTest {
    private static boolean setUpIsDone = false;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private BasketService basketService;

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    @Autowired
    public BasketEndPointTest() {

    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void addProductToBasketTest() throws Exception {
        this.mvc.perform(post("/api/baskets").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new PurchaseRequest(1, "nameOfProduct", 1000, 500000)))).andExpect(status().isOk());
    }

    @Test
    public void deleteProductTest() throws Exception {
        this.mvc.perform(delete("/api/baskets/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void editProductFromBasketTest() throws Exception {
        this.mvc.perform(put("/api/baskets").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new PurchaseRequest(1, "nameOfProduct", 1000, 500000)))).andExpect(status().isOk());
    }

    @Test
    public void getProductsFromBasketTest() throws Exception {
        this.mvc.perform(get("/api/baskets").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
