package net.thumbtack.onlineshop.endpointtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.rest.PurchaseEndPoint;
import net.thumbtack.onlineshop.services.PurchaseService;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PurchaseEndPoint.class)
public class PurchaseEndPointTest {
    private static boolean setUpIsDone = false;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private PurchaseService purchaseService;

    private CommonDaoImpl commonDao = new CommonDaoImpl();

    @Autowired
    public PurchaseEndPointTest() {

    }

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() throws ServerException {
        commonDao.clear();
    }

    @Test
    public void purchaseTest() throws Exception {
        this.mvc.perform(post("/api/purchases").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new PurchaseRequest(1, "product1", 1000, 100)))).andExpect(status().isOk());
    }

    @Test
    public void purchaseBasketTest() throws Exception {
        this.mvc.perform(post("/api/purchases/baskets").contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(new PurchaseRequest[3]))).andExpect(status().isOk());
    }

    @Test
    public void getPurchasesOfClientTest() throws Exception {
        this.mvc.perform(get("/api/purchases/clients/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void getClientsBuyingProductsTest() throws Exception {
        this.mvc.perform(get("/api/purchases/products/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
