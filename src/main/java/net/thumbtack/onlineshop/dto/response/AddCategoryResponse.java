package net.thumbtack.onlineshop.dto.response;

import java.util.Objects;

public class AddCategoryResponse {
    int id;
    String name;
    int parentId;
    String parentName;

    public AddCategoryResponse() {
    }

    public AddCategoryResponse(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public AddCategoryResponse(int id, String name, int parentId, String parentName) {
        this(id, name);
        this.parentId = parentId;
        this.parentName = parentName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryResponse)) return false;
        AddCategoryResponse that = (AddCategoryResponse) o;
        return getId() == that.getId() &&
                getParentId() == that.getParentId() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getParentName(), that.getParentName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getParentId(), getParentName());
    }
}
