package net.thumbtack.onlineshop.dto.response;

import java.util.Objects;

public class RegisterAdminResponse {
    private int id;
    private String firstName;
    private String lastName;
    private String patronymicName;
    private String position;

    public RegisterAdminResponse(int id, String firstName, String lastName, String position) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
    }

    public RegisterAdminResponse(int id, String firstName, String lastName, String patronymicName, String position) {
        this(id, firstName, lastName, position);
        this.patronymicName = patronymicName;
    }

    public RegisterAdminResponse() {

    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegisterAdminResponse)) return false;
        RegisterAdminResponse that = (RegisterAdminResponse) o;
        return getId() == that.getId() &&
                Objects.equals(getFirstName(), that.getFirstName()) &&
                Objects.equals(getLastName(), that.getLastName()) &&
                Objects.equals(getPatronymicName(), that.getPatronymicName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getPatronymicName());
    }
}
