package net.thumbtack.onlineshop.dto.response;

import java.util.List;
import java.util.Objects;

public class GetInfoProductResponse {
    private int id;
    private String name;
    private int count;
    private int price;
    private List<String> categories;

    public GetInfoProductResponse() {
    }

    public GetInfoProductResponse(int id, String name, int count, int price, List<String> categories) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.price = price;
        this.categories = categories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetInfoProductResponse)) return false;
        GetInfoProductResponse that = (GetInfoProductResponse) o;
        return getId() == that.getId() &&
                getCount() == that.getCount() &&
                getPrice() == that.getPrice() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getCategories(), that.getCategories());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getCount(), getPrice(), getCategories());
    }
}
