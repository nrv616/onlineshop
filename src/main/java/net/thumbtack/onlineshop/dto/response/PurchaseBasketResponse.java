package net.thumbtack.onlineshop.dto.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PurchaseBasketResponse {
    private List<PurchaseResponse> bought = new ArrayList<>();
    private List<PurchaseResponse> remaining = new ArrayList<>();

    public PurchaseBasketResponse() {
    }

    public void addBought(PurchaseResponse purchaseResponse) {
        bought.add(purchaseResponse);
    }

    public void addRemaining(PurchaseResponse purchaseResponse) {
        remaining.add(purchaseResponse);
    }

    public List<PurchaseResponse> getBought() {
        return bought;
    }

    public List<PurchaseResponse> getRemaining() {
        return remaining;
    }

    public void setBought(List<PurchaseResponse> bought) {
        this.bought = bought;
    }

    public void setRemaining(List<PurchaseResponse> remaining) {
        this.remaining = remaining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PurchaseBasketResponse)) return false;
        PurchaseBasketResponse that = (PurchaseBasketResponse) o;
        return Objects.equals(getBought(), that.getBought()) &&
                Objects.equals(getRemaining(), that.getRemaining());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBought(), getRemaining());
    }
}
