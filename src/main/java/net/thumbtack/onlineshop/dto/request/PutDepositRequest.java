package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.DepositValue;

import java.util.Objects;

public class PutDepositRequest {
    @DepositValue
    private int deposit;

    public PutDepositRequest() {
    }

    public PutDepositRequest(int deposit) {
        this.deposit = deposit;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PutDepositRequest)) return false;
        PutDepositRequest that = (PutDepositRequest) o;
        return getDeposit() == that.getDeposit();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDeposit());
    }
}
