package net.thumbtack.onlineshop.dto.request;

import java.util.Objects;

public class EditCategoryRequest {
    String name;
    Integer parentId;

    public EditCategoryRequest() {
    }

    public EditCategoryRequest(String name) {
        this.name = name;
    }

    public EditCategoryRequest(String name, Integer parentId) {
        this.name = name;
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryRequest)) return false;
        AddCategoryRequest that = (AddCategoryRequest) o;
        return getParentId() == that.getParentId() &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getParentId());
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
