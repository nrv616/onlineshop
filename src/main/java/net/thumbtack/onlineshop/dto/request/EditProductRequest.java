package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.MaxSize;

import java.util.List;
import java.util.Objects;


public class EditProductRequest {
    @MaxSize
    private String name;
    private Integer price;
    private Integer count;
    private List<Integer> categories;

    public EditProductRequest() {
    }

    public EditProductRequest(String name, Integer price, Integer count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public EditProductRequest(String name, Integer price, Integer count, List<Integer> categories) {
        this(name, price, count);
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EditProductRequest)) return false;
        EditProductRequest that = (EditProductRequest) o;
        return getPrice() == that.getPrice() &&
                getCount() == that.getCount() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getCategories(), that.getCategories());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPrice(), getCount(), getCategories());
    }
}
