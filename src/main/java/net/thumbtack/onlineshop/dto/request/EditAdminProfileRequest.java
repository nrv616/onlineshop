package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.MaxSize;
import net.thumbtack.onlineshop.validator.MinSize;
import net.thumbtack.onlineshop.validator.NameContent;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class EditAdminProfileRequest {
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String firstName;
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String lastName;
    @NameContent
    @MaxSize
    private String patronymicName;
    @MaxSize
    @NotBlank
    @NotNull
    private String position;
    @MaxSize
    @NotBlank
    @NotNull
    @MinSize
    private String oldPassword;
    @MaxSize
    @NotBlank
    @NotNull
    @MinSize
    private String newPassword;

    public EditAdminProfileRequest() {

    }

    public EditAdminProfileRequest(String firstName, String lastName, String patronymicName, String position, String oldPassword, String newPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymicName = patronymicName;
        this.position = position;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EditAdminProfileRequest)) return false;
        EditAdminProfileRequest that = (EditAdminProfileRequest) o;
        return Objects.equals(getFirstName(), that.getFirstName()) &&
                Objects.equals(getLastName(), that.getLastName()) &&
                Objects.equals(getPatronymicName(), that.getPatronymicName()) &&
                Objects.equals(getPosition(), that.getPosition()) &&
                Objects.equals(getOldPassword(), that.getOldPassword()) &&
                Objects.equals(getNewPassword(), that.getNewPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getPatronymicName(), getPosition(), getOldPassword(), getNewPassword());
    }
}
