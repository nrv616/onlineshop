package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.MaxSize;
import net.thumbtack.onlineshop.validator.MinSize;
import net.thumbtack.onlineshop.validator.NameContent;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class EditClientProfileRequest {
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String firstName;
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String lastName;
    @NameContent
    @MaxSize
    private String patronymicName;
    @Email
    @NotBlank
    @NotNull
    @MaxSize
    private String email;
    @NotBlank
    @NotNull
    @MaxSize
    private String address;
    @NotBlank
    @NotNull
    @MaxSize
    private String phone;
    @MaxSize
    @NotBlank
    @NotNull
    @MinSize
    private String oldPassword;
    @MaxSize
    @NotBlank
    @NotNull
    @MinSize
    private String newPassword;

    public EditClientProfileRequest() {
    }

    public EditClientProfileRequest(@NotBlank @NotNull String firstName, @NotBlank @NotNull String lastName, String patronymicName, @Email @NotBlank @NotNull String email, @NotBlank @NotNull String address, @NotBlank @NotNull String phone, @NotBlank @NotNull String oldPassword, @NotBlank @NotNull String newPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymicName = patronymicName;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EditClientProfileRequest)) return false;
        EditClientProfileRequest that = (EditClientProfileRequest) o;
        return Objects.equals(getFirstName(), that.getFirstName()) &&
                Objects.equals(getLastName(), that.getLastName()) &&
                Objects.equals(getPatronymicName(), that.getPatronymicName()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getPhone(), that.getPhone()) &&
                Objects.equals(getOldPassword(), that.getOldPassword()) &&
                Objects.equals(getNewPassword(), that.getNewPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getPatronymicName(), getEmail(), getAddress(), getPhone(), getOldPassword(), getNewPassword());
    }
}
