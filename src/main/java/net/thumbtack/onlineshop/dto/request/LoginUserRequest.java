package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.ContainsWordsAndNumbersOnly;
import net.thumbtack.onlineshop.validator.MaxSize;
import net.thumbtack.onlineshop.validator.MinSize;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class LoginUserRequest {
    @MaxSize
    @NotBlank
    @NotNull
    @ContainsWordsAndNumbersOnly
    private String login;
    @MaxSize
    @NotBlank
    @NotNull
    @MinSize
    private String password;

    public LoginUserRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public LoginUserRequest() {

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginUserRequest)) return false;
        LoginUserRequest that = (LoginUserRequest) o;
        return Objects.equals(getLogin(), that.getLogin()) &&
                Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLogin(), getPassword());
    }
}
