package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.ContainsWordsAndNumbersOnly;
import net.thumbtack.onlineshop.validator.MaxSize;
import net.thumbtack.onlineshop.validator.MinSize;
import net.thumbtack.onlineshop.validator.NameContent;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class RegisterAdminRequest {
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String firstName;
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String lastName;
    @NameContent
    @MaxSize
    private String patronymicName;
    @MaxSize
    @NotBlank
    @NotNull
    @ContainsWordsAndNumbersOnly
    private String login;
    @MaxSize
    @NotBlank
    @NotNull
    @MinSize
    private String password;
    @MaxSize
    @NotBlank
    @NotNull
    private String position;

    public RegisterAdminRequest(String firstName, String lastName, String login, String password, String position) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
        this.position = position;
    }

    public RegisterAdminRequest(String firstName, String lastName, String patronymicName, String login, String password, String position) {
        this(firstName, lastName, login, password, position);
        this.patronymicName = patronymicName;
    }

    public RegisterAdminRequest() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegisterAdminRequest)) return false;
        RegisterAdminRequest that = (RegisterAdminRequest) o;
        return Objects.equals(getFirstName(), that.getFirstName()) &&
                Objects.equals(getLastName(), that.getLastName()) &&
                Objects.equals(getPatronymicName(), that.getPatronymicName()) &&
                Objects.equals(getLogin(), that.getLogin()) &&
                Objects.equals(getPassword(), that.getPassword()) &&
                Objects.equals(getPosition(), that.getPosition());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getPatronymicName(), getLogin(), getPassword(), getPosition());
    }
}
