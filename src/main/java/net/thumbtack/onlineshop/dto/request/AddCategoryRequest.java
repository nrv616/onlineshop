package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.MaxSize;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class AddCategoryRequest {
    @MaxSize
    @NotBlank
    @NotNull
    String name;
    int parentId;

    public AddCategoryRequest() {
    }

    public AddCategoryRequest(String name, int parentId) {
        this.name = name;
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryRequest)) return false;
        AddCategoryRequest that = (AddCategoryRequest) o;
        return getParentId() == that.getParentId() &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getParentId());
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }
}
