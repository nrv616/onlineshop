package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.MaxSize;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

public class AddProductRequest {
    @MaxSize
    @NotBlank
    @NotNull
    private String name;
    private int price;
    private int count;
    private List<Integer> categories;

    public AddProductRequest() {
    }

    public AddProductRequest(String name, int price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public AddProductRequest(String name, int price, int count, List<Integer> categories) {
        this(name, price, count);
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddProductRequest)) return false;
        AddProductRequest that = (AddProductRequest) o;
        return getPrice() == that.getPrice() &&
                getCount() == that.getCount() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getCategories(), that.getCategories());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPrice(), getCount(), getCategories());
    }
}
