package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class RegisterClientRequest {
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String firstName;
    @NameContent
    @MaxSize
    @NotBlank
    @NotNull
    private String lastName;
    @NameContent
    @MaxSize
    private String patronymicName;
    @MaxSize
    @NotBlank
    @NotNull
    @ContainsWordsAndNumbersOnly
    private String login;
    @MaxSize
    @NotBlank
    @NotNull
    @MinSize
    private String password;
    @Email
    @NotBlank
    @NotNull
    @MaxSize
    private String email;
    @NotBlank
    @NotNull
    @MaxSize
    private String address;
    @Phone
    @NotBlank
    @NotNull
    @MaxSize
    private String phone;

    public RegisterClientRequest() {

    }

    public RegisterClientRequest(String firstName, String lastName, String patronymicName, String login, String password, String email, String address, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymicName = patronymicName;
        this.login = login;
        this.password = password;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegisterClientRequest)) return false;
        RegisterClientRequest that = (RegisterClientRequest) o;
        return Objects.equals(getFirstName(), that.getFirstName()) &&
                Objects.equals(getLastName(), that.getLastName()) &&
                Objects.equals(getPatronymicName(), that.getPatronymicName()) &&
                Objects.equals(getLogin(), that.getLogin()) &&
                Objects.equals(getPassword(), that.getPassword()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getPhone(), that.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getPatronymicName(), getLogin(), getPassword(), getEmail(), getAddress(), getPhone());
    }
}
