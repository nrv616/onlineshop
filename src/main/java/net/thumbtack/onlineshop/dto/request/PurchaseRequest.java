package net.thumbtack.onlineshop.dto.request;

import net.thumbtack.onlineshop.validator.MaxSize;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class PurchaseRequest {
    private int id;
    @MaxSize
    @NotBlank
    @NotNull
    private String name;
    private int price;
    private int count;

    public PurchaseRequest() {
    }

    public PurchaseRequest(int id, String name, int price, int count) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public PurchaseRequest(int id, String name, int price) {
        this(id, name, price, 1);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PurchaseRequest)) return false;
        PurchaseRequest that = (PurchaseRequest) o;
        return getId() == that.getId() &&
                getPrice() == that.getPrice() &&
                getCount() == that.getCount() &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPrice(), getCount());
    }
}
