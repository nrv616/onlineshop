package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.ProductDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.Product;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductDaoImpl extends DaoImplBase implements ProductDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDaoImpl.class);

    @Override
    public Product addProduct(Product product) throws ServerException {
        LOGGER.debug("DAO add Product {}", product.getName());
        try (SqlSession sqlSession = getSession()) {
            try {
                getProductMapper(sqlSession).addProduct(product);
                sqlSession.commit();
                return product;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't add Product {}, {}", product.getName(), ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public void addProductToCategories(List<Integer> categories, int id) throws ServerException {
        LOGGER.debug("DAO add Product {} to Categories {}", id, categories);
        try (SqlSession sqlSession = getSession()) {
            try {
                getProductMapper(sqlSession).addProductToCategories(categories, id);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't add Product {} to Categories {} {}", id, categories, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public void editProduct(Product product, List<Integer> categories) throws ServerException {
        LOGGER.debug("DAO edit Product {}", product.getName());
        try (SqlSession sqlSession = getSession()) {
            try {
                int version = getProductMapper(sqlSession).getVersionOfProduct(product.getId());
                getProductMapper(sqlSession).editProduct(product.getId(), product.getName(), product.getCount(), product.getPrice(), version);
                if (categories != null) {
                    getProductMapper(sqlSession).deleteCategories(product.getId());
                    getProductMapper(sqlSession).addProductToCategories(categories, product.getId());
                }
                product.setCategories(getProductMapper(sqlSession).getCategoriesIdOfProductById(product.getId()));
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't edit Product {}, {}", product.getName(), ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public void deleteProduct(int id) throws ServerException {
        LOGGER.debug("DAO delete Product {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                getProductMapper(sqlSession).deleteProductById(id);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Product {}, {}", id, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public Product getInfoProduct(int id) throws ServerException {
        LOGGER.debug("DAO get Info Product {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                Product product = getProductMapper(sqlSession).getProductById(id);
                List<Category> categories = getProductMapper(sqlSession).getCategoriesOfProductById(id);
                return new Product(product.getId(), product.getName(), product.getPrice(), product.getCount(), categories);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Info Product {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getProductsByCategoriesOrderProduct(List<Integer> categories) throws ServerException {
        LOGGER.debug("DAO get Products by Categories {}", categories);
        try (SqlSession sqlSession = getSession()) {
            List<Product> result = new ArrayList<>();
            try {
                List<Product> products = getProductMapper(sqlSession).getProductsWithIdOfCategoriesById(categories);
                for (Product product : products) {
                    List<Category> categories1 = getProductMapper(sqlSession).getCategoriesNamesOfProductById(product.getId());
                    result.add(new Product(product.getId(), product.getName(), product.getCount(), product.getPrice(), categories1));
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Products by Categories {}, {}", categories, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
            return result;
        }
    }

    @Override
    public List<Product> getProductsByCategoriesOrderCategory(List<Integer> categories) throws ServerException {
        LOGGER.debug("DAO get Products by Categories {}", categories);
        try (SqlSession sqlSession = getSession()) {
            List<Product> result = new ArrayList<>();
            try {
                List<Category> categories1 = getCategoryMapper(sqlSession).getCategoriesById(categories);
                for (Category category : categories1) {
                    List<Product> products = getProductMapper(sqlSession).getProductsWithIdOfCategoryById(category.getId());
                    List<Category> categoryName = new ArrayList<>();
                    categoryName.add(category);
                    for (Product product : products) {
                        result.add(new Product(product.getId(), product.getName(), product.getCount(), product.getPrice(), categoryName));
                    }
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Products by Categories {}, {}", categories, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
            return result;
        }
    }

    @Override
    public List<Product> getProducts() throws ServerException {
        LOGGER.debug("DAO get Products order Product{}");
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Product> products = getProductMapper(sqlSession).getProducts();
                for (Product product : products) {
                    List<Category> categories = getProductMapper(sqlSession).getCategoriesNamesOfProductById(product.getId());
                    product.setCategories(categories);
                }
                return products;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Products order Product {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getProductsOrderCategory() throws ServerException {
        LOGGER.debug("DAO get Products order Category {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Product> products = getProductMapper(sqlSession).getProducts();
                List<Category> categories = getProductMapper(sqlSession).getCategoriesOfProducts();
                Set<Integer> productsWithCategory = new HashSet<>(getProductMapper(sqlSession).getProductsId());
                List<Product> result = new ArrayList<>();
                for (Product product : products) {
                    if (!productsWithCategory.contains(product.getId())) {
                        result.add(product);
                    }
                }
                result.sort(Comparator.comparing(Product::getName));
                for (Category category : categories) {
                    List<Product> products1 = getProductMapper(sqlSession).getProductsWithIdOfCategoryById(category.getId());
                    List<Category> categories1 = new ArrayList<>();
                    categories1.add(category);
                    for (Product product : products1) {
                        result.add(new Product(product.getId(), product.getName(), product.getCount(), product.getPrice(), categories1));
                    }
                }
                return result;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Products order Category {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getProductsWithoutCategories(String order) throws ServerException {
        LOGGER.debug("DAO get Products {}");
        try (SqlSession sqlSession = getSession()) {
            List<Product> products = getProductMapper(sqlSession).getProducts();
            try {
                Set<Integer> productsWithCategory = new HashSet<>(getProductMapper(sqlSession).getProductsId());
                List<Product> productsWithoutCategory = new ArrayList<>();
                for (Product product : products) {
                    if (!productsWithCategory.contains(product.getId())) {
                        productsWithoutCategory.add(product);
                    }
                }
                return productsWithoutCategory;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Products {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }
}
