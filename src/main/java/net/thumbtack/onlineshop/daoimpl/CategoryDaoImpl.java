package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.CategoryDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Category;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryDaoImpl extends DaoImplBase implements CategoryDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDaoImpl.class);

    @Override
    public Category addCategory(Category category) throws ServerException {
        LOGGER.debug("DAO add Category {}", category.getName());
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).addCategory(category);
                sqlSession.commit();
                return new Category(category.getId(), category.getName());
            } catch (RuntimeException ex) {
                LOGGER.info("Can't add Category {}, {}", category.getName(), ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public Category addSubCategory(Category category) throws ServerException {
        LOGGER.debug("DAO add Category {}", category);
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).addSubCategory(category, category.getParentId());
                Category parentCategory = getCategoryMapper(sqlSession).getCategoryById(category.getParentId());
                sqlSession.commit();
                return new Category(category.getId(), category.getName(), parentCategory);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't add Category {}, {}", ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public Category getCategoryById(int id) throws ServerException {
        LOGGER.debug("DAO get Category {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                Category category = getCategoryMapper(sqlSession).getCategoryById(id);
                try {
                    int parentId = getCategoryMapper(sqlSession).getParentIdById(id);
                    Category parentCategory = getCategoryMapper(sqlSession).getCategoryById(parentId);
                    return new Category(category.getId(), category.getName(), parentCategory);
                } catch (RuntimeException e) {
                    return new Category(category.getId(), category.getName());
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Category {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.NONEXISTENT_CATEGORY, "id");
            }
        }
    }

    @Override
    public Category editCategoryById(int id, String name, int parentId) throws ServerException {
        LOGGER.debug("DAO edit Category {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                try {
                    getCategoryMapper(sqlSession).editCategoryById(id, name, parentId);
                    Category parent = getCategoryMapper(sqlSession).getCategoryById(parentId);
                    sqlSession.commit();
                    return new Category(id, name, parent);
                } catch (RuntimeException e) {
                    sqlSession.commit();
                    return new Category(id, name);
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Category {}, {}", id, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public void deleteCategoryById(int id) throws ServerException {
        LOGGER.debug("DAO delete Category {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).deleteCategoryById(id);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Category {}, {}", id, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.NONEXISTENT_CATEGORY, "id");
            }
        }
    }

    @Override
    public List<Category> getCategories() throws ServerException {
        LOGGER.debug("DAO get Categories {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                return getCategoryMapper(sqlSession).getCategories();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Categories {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }
}