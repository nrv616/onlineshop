package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.AdminDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AdminDaoImpl extends DaoImplBase implements AdminDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminDaoImpl.class);

    @Override
    public void registerAdmin(Admin admin, String javaSessionId) throws ServerException {
        LOGGER.debug("DAO insert Admin {}", admin);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).register(admin);
                getAdminMapper(sqlSession).registerAdmin(admin, admin.getId());
                getUserMapper(sqlSession).logIn(javaSessionId, admin.getId());
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Admin {}, {}", admin, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.INVALID_REGISTER_TRY, "login");
            }
        }
    }

    @Override
    public void editAdminProfile(Admin admin) throws ServerException {
        LOGGER.debug("DAO edit profile Admin {}", admin);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).editAdminProfile(admin);
                getAdminMapper(sqlSession).editAdminProfile(admin.getId(), admin.getPosition());
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't edit profile Admin {}, {}", admin, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public Admin getAdmin(int id) throws ServerException {
        LOGGER.debug("DAO get Admin {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getAdminMapper(sqlSession).getAdmin(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Admin {}, {}", id, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "login");
            }
        }
    }

    @Override
    public Admin getAdminByCookie(String cookie) throws ServerException {
        LOGGER.debug("DAO get Admin {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getAdminMapper(sqlSession).getAdminByCookie(cookie);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Admin {}, {}", cookie, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "login");
            }
        }
    }
}
