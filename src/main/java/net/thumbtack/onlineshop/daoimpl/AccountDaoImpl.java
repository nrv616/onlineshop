package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.AccountDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.User;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AccountDaoImpl extends DaoImplBase implements AccountDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountDaoImpl.class);

    @Override
    public User getUserInfo(String cookie) throws ServerException {
        LOGGER.debug("DAO get info about User {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            try {
                int userId = getAccountMapper(sqlSession).getUserIdByJavaSessionId(cookie);
                return getAccountMapper(sqlSession).getUserById(userId);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get info about User {}, {}", cookie, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }
}
