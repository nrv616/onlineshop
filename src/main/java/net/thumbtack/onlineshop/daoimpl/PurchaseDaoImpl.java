package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.PurchaseDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.Product;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PurchaseDaoImpl extends DaoImplBase implements PurchaseDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseDaoImpl.class);

    @Override
    public void purchase(Client client, int id, String name, int price, int count, int newDeposit, int newCount) throws ServerException {
        LOGGER.debug("DAO purchase Product {}", name);
        try (SqlSession sqlSession = getSession()) {
            try {
                int versionOfDeposit = getDepositMapper(sqlSession).getVersionOfDeposit(client.getId());
                int version = getProductMapper(sqlSession).getVersionOfProduct(id);
                int numberOfChanges = getDepositMapper(sqlSession).putDepositPurchase(client.getId(), newDeposit, versionOfDeposit);
                if (numberOfChanges == 0) {
                    LOGGER.info("Can't purchase Product");
                    sqlSession.rollback();
                    throw new ServerException(ServerErrorCode.INVALID_PURSCHASE_TRY, "version");
                }
                getPurchaseMapper(sqlSession).purchase(client.getId(), id, name, price, count);
                numberOfChanges = getProductMapper(sqlSession).setNewCount(id, newCount, version, version + 1);
                if (numberOfChanges == 0) {
                    LOGGER.info("Can't purchase Product {}", name);
                    sqlSession.rollback();
                    throw new ServerException(ServerErrorCode.INVALID_PURSCHASE_TRY, "version");
                }
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't purchase Product {}, {}", name, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> purchaseBasket(Client client, List<Product> products) throws ServerException {
        LOGGER.debug("DAO purchase Products from Basket {}", client.getId());
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Product> result = new ArrayList<>();
                int basketId = getBasketMapper(sqlSession).getBasketId(client.getId());
                int newDeposit = getDepositMapper(sqlSession).getDeposit(client.getId());
                for (Product productTemp : products) {
                    Product product = getProductMapper(sqlSession).getProductById(productTemp.getId());
                    if (product == null) {
                        throw new ServerException(ServerErrorCode.NONEXISTENT_PRODUCT, "id");
                    }
                    Integer count = getBasketMapper(sqlSession).getCount(basketId, productTemp.getId());
                    if (productTemp.getCount() == 0) {
                        productTemp.setCount(count);
                    }
                    if (product.getCount() < productTemp.getCount()) {
                        throw new ServerException(ServerErrorCode.INVALID_COUNT_PRODUCT, "count");
                    }
                    if (!productTemp.getName().equals(product.getName())) {
                        throw new ServerException(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE, "name");
                    }
                    if (productTemp.getPrice() != product.getPrice()) {
                        throw new ServerException(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE, "price");
                    }
                    int newCount = count - productTemp.getCount();
                    newDeposit -= count * productTemp.getPrice();
                    getPurchaseMapper(sqlSession).purchase(client.getId(), product.getId(), product.getName(), product.getPrice(), productTemp.getCount());
                    if (newCount <= 0) {
                        getPurchaseMapper(sqlSession).purchaseBasket(basketId, product.getId(), productTemp.getCount());
                    } else {
                        getPurchaseMapper(sqlSession).purchaseBasket(basketId, product.getId(), newCount);
                    }
                    if (newCount == 0) {
                        getBasketMapper(sqlSession).deleteProduct(basketId, product.getId());
                    }
                    int version = getProductMapper(sqlSession).getVersionOfProduct(productTemp.getId());
                    int numberOfChanges = getProductMapper(sqlSession).setNewCount(product.getId(), product.getCount() - productTemp.getCount(), version, version + 1);
                    if (numberOfChanges == 0) {
                        LOGGER.info("Can't purchase Product {}", product.getName());
                        sqlSession.rollback();
                        throw new ServerException(ServerErrorCode.INVALID_PURSCHASE_TRY, "version");
                    }
                    result.add(new Product(product.getId(), product.getName(), product.getPrice(), productTemp.getCount()));
                }
                if (newDeposit < 0) {
                    throw new ServerException(ServerErrorCode.INVALID_DEPOSIT_VALUE, "deposit");
                }
                int versionOfDeposit = getDepositMapper(sqlSession).getVersionOfDeposit(client.getId());
                int numberOfChanges = getDepositMapper(sqlSession).putDepositPurchase(client.getId(), newDeposit, versionOfDeposit);
                if (numberOfChanges == 0) {
                    LOGGER.info("Can't purchase Product");
                    sqlSession.rollback();
                    throw new ServerException(ServerErrorCode.INVALID_PURSCHASE_TRY, "version");
                }
                sqlSession.commit();
                return result;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't purchase Products from Basket {}, {}", client.getId(), ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getPurchasesOfClient(int id) throws ServerException {
        LOGGER.debug("DAO get Purchases of Client {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getProductsByClientId(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Purchases of Client {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getPurchasesOfClientLimit(int id, int offset, int limit) throws ServerException {
        LOGGER.debug("DAO get Purchases of Client {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getProductsByClientIdLimit(id, offset, limit);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Purchases of Client {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getPurchasesOfClients(List<Integer> clientsId) throws ServerException {
        LOGGER.debug("DAO get Purchases of Clients {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getPurchasesByClientsId(clientsId);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Purchases of Clients {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getPurchasesOfClientsLimit(List<Integer> clientsId, int offset, int limit) throws ServerException {
        LOGGER.debug("DAO get Purchases of Clients {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getPurchasesByClientsIdLimit(clientsId, offset, limit);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Purchases of Clients {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Client> getClientsBuyingProduct(int id) throws ServerException {
        LOGGER.debug("DAO get Clients buying Product {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getClientsByProduct(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Clients buying Product {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Client> getClientsBuyingProductLimit(int id, int offset, int limit) throws ServerException {
        LOGGER.debug("DAO get Clients buying Product {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getClientsByProductLimit(id, offset, limit);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Clients buying Product {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Client> getClientsBuyingProducts(List<Integer> productsId) throws ServerException {
        LOGGER.debug("DAO get Clients buying Products {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getClientsByProducts(productsId);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Clients buying Products {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Client> getClientsBuyingProductsLimit(List<Integer> productsId, int offset, int limit) throws ServerException {
        LOGGER.debug("DAO get Clients buying Products {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                return getPurchaseMapper(sqlSession).getClientsByProductsLimit(productsId, offset, limit);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Clients buying Products {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public int getDeposit(String javaSessionId) throws ServerException {
        LOGGER.debug("DAO get Deposit {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                int userId = getUserMapper(sqlSession).getUserIdByJavaSessionId(javaSessionId);
                return getDepositMapper(sqlSession).getDeposit(userId);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Deposit {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }
}
