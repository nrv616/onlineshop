package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.UserDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.User;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserDaoImpl extends DaoImplBase implements UserDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public User logIn(String login, String password, String javaSessionId) throws ServerException {
        LOGGER.debug("DAO login User {}", login);
        try (SqlSession sqlSession = getSession()) {
            try {
                User user = getUserMapper(sqlSession).getUser(login, password);
                getUserMapper(sqlSession).logIn(javaSessionId, user.getId());
                sqlSession.commit();
                return user;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't login User {}, {}", login, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public void logOut(String cookie) throws ServerException {
        LOGGER.debug("DAO logOut User {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).logOut(cookie);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't logOut User {}, {}", cookie, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public User getUserByCookie(String cookie) throws ServerException {
        LOGGER.debug("DAO getUserByCookie {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getUserMapper(sqlSession).getUserByJavaSessionId(cookie);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't logOut User {}, {}", cookie, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }
}
