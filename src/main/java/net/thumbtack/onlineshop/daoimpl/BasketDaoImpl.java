package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.BasketDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.Product;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BasketDaoImpl extends DaoImplBase implements BasketDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(BasketDaoImpl.class);

    @Override
    public int addProductToBasket(Client client, int id, String name, int price, int count) throws ServerException {
        LOGGER.debug("DAO add Product to Basket {}", name);
        try (SqlSession sqlSession = getSession()) {
            try {
                int basketId = getBasketMapper(sqlSession).getBasketId(client.getId());
                getBasketMapper(sqlSession).addProductToBasket(basketId, id, count);
                sqlSession.commit();
                return basketId;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't add Product to Basket {}, {}", name, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public void deleteProduct(Client client, int id) throws ServerException {
        LOGGER.debug("DAO delete Product from Basket {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                int basketId = getBasketMapper(sqlSession).getBasketId(client.getId());
                getBasketMapper(sqlSession).deleteProduct(basketId, id);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Product from Basket {}, {}", id, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public int editProductFromBasket(Client client, int id, String name, int price, int count) throws ServerException {
        LOGGER.debug("DAO edit Product from Basket {}", name);
        try (SqlSession sqlSession = getSession()) {
            try {
                int basketId = getBasketMapper(sqlSession).getBasketId(client.getId());
                getBasketMapper(sqlSession).editProductFromBasket(basketId, id, count);
                sqlSession.commit();
                return basketId;
            } catch (RuntimeException ex) {
                LOGGER.info("Can't edit Product from Basket {}, {}", name, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getProductsFromBasket(Client client) throws ServerException {
        LOGGER.debug("DAO get Products from Basket {}", client);
        try (SqlSession sqlSession = getSession()) {
            try {
                int basketId = getBasketMapper(sqlSession).getBasketId(client.getId());
                return getBasketMapper(sqlSession).getProductsInBasket(basketId);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Products from Basket {}, {}", client, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public List<Product> getProductsFromBasketById(int id) throws ServerException {
        LOGGER.debug("DAO get Products from Basket {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getBasketMapper(sqlSession).getProductsInBasket(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Products from Basket {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public Product getProductById(int id) throws ServerException {
        LOGGER.debug("DAO get Product {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getProductMapper(sqlSession).getProductById(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Product {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }
}
