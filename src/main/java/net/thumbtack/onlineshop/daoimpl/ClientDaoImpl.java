package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.ClientDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientDaoImpl extends DaoImplBase implements ClientDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientDaoImpl.class);

    @Override
    public void registerClient(Client client, String javaSessionId) throws ServerException {
        LOGGER.debug("DAO insert Client {}", client);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).registerCl(client);
                getClientMapper(sqlSession).registerClient(client, client.getId());
                getDepositMapper(sqlSession).createDeposit(client.getId());
                getBasketMapper(sqlSession).createBasket(client.getId());
                getUserMapper(sqlSession).logIn(javaSessionId, client.getId());
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Client {}, {}", client, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.INVALID_REGISTER_TRY, "login");
            }
        }
    }

    @Override
    public List<Client> getClients() throws ServerException {
        LOGGER.debug("DAO get all Clients {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                return getClientMapper(sqlSession).getClients();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get all Clients {}, {}", ex);
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public void editClientProfile(Client client) throws ServerException {
        LOGGER.debug("DAO edit profile Client {}", client);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).editClientProfile(client);
                getClientMapper(sqlSession).editClientProfile(client.getId(), client.getEmail(), client.getAddress(), client.getPhone());
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't edit profile Client {}, {}", client, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }

    @Override
    public Client getClient(int id) throws ServerException {
        LOGGER.debug("DAO get Client {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getClientMapper(sqlSession).getClient(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Client {}, {}", id, ex);
                throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "login");
            }
        }
    }

    @Override
    public Client getClientByCookie(String cookie) throws ServerException {
        LOGGER.debug("DAO get Client {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            try {
                return getClientMapper(sqlSession).getClientByCookie(cookie);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Client {}, {}", cookie, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "login");
            }
        }
    }
}
