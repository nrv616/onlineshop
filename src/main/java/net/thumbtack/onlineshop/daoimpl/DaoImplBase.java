package net.thumbtack.onlineshop.daoimpl;


import net.thumbtack.onlineshop.mappers.*;
import net.thumbtack.onlineshop.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

public class DaoImplBase {
    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected AdminMapper getAdminMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(AdminMapper.class);
    }

    protected ClientMapper getClientMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ClientMapper.class);
    }

    protected UserMapper getUserMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(UserMapper.class);
    }

    protected CategoryMapper getCategoryMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(CategoryMapper.class);
    }

    protected ProductMapper getProductMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ProductMapper.class);
    }

    protected DepositMapper getDepositMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(DepositMapper.class);
    }

    protected BasketMapper getBasketMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(BasketMapper.class);
    }

    protected PurchaseMapper getPurchaseMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PurchaseMapper.class);
    }

    protected AccountMapper getAccountMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(AccountMapper.class);
    }
}