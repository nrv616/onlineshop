package net.thumbtack.onlineshop.daoimpl;

import net.thumbtack.onlineshop.dao.DepositDao;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DepositDaoImpl extends DaoImplBase implements DepositDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DepositDaoImpl.class);

    @Override
    public void putDeposit(Client client, int deposit) throws ServerException {
        LOGGER.debug("DAO put Deposit {}", deposit);
        try (SqlSession sqlSession = getSession()) {
            try {
                int version = getDepositMapper(sqlSession).getVersionOfDeposit(client.getId());
                int numberOfChanges = getDepositMapper(sqlSession).putDeposit(client.getId(), getDepositMapper(sqlSession).getDeposit(client.getId()) + deposit, version);
                if (numberOfChanges == 0) {
                    LOGGER.info("Can't put Deposit");
                    sqlSession.rollback();
                    throw new ServerException(ServerErrorCode.INVALID_PUT_DEPOSIT_TRY, "version");
                }
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't put Deposit {}, {}", deposit, ex);
                sqlSession.rollback();
                throw new ServerException(ServerErrorCode.DATABASE_ERROR, "dataBase");
            }
        }
    }
}