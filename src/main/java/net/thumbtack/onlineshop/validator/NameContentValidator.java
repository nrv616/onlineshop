package net.thumbtack.onlineshop.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class NameContentValidator implements ConstraintValidator<NameContent, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s != null) {
            return Pattern.matches("^[А-Яа-я\\s-]*$", s);
        } else {
            return true;
        }
    }
}
