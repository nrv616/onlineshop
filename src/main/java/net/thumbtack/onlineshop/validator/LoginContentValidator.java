package net.thumbtack.onlineshop.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class LoginContentValidator implements ConstraintValidator<ContainsWordsAndNumbersOnly, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s != null) {
            return Pattern.matches("^[A-Za-zА-Яа-я0-9]*$", s);
        } else {
            return false;
        }
    }
}
