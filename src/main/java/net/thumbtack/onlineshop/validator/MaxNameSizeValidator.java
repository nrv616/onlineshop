package net.thumbtack.onlineshop.validator;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MaxNameSizeValidator implements ConstraintValidator<MaxSize, String> {
    @Value("${max_name_length}")
    private int maxNameLength;

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s != null) {
            return s.length() <= maxNameLength;
        } else {
            return true;
        }
    }
}
