package net.thumbtack.onlineshop.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DepositValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DepositValue {
    String message() default "Invalid deposit value";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
