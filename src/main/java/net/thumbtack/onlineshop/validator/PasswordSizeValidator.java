package net.thumbtack.onlineshop.validator;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordSizeValidator implements ConstraintValidator<MinSize, String> {
    @Value("${min_password_length}")
    private int minPasswordLength;

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s != null) {
            return s.length() >= minPasswordLength;
        } else {
            return false;
        }
    }
}
