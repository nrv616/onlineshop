package net.thumbtack.onlineshop.exception;

public enum ServerErrorCode {
    INVALID_JAVASESSIONID("Not indicated javaSessionId"),
    INVALID_FIRSTNAME("Not indicated firstname"),
    INVALID_LASTNAME("Not indicated lastname"),
    INVALID_LOGIN("Not indicated login"),
    INVALID_PASSWORD("Not indicated password"),
    INVALID_POSITION("Not indicated position"),
    INVALID_EMAIL("Not indicated email"),
    INVALID_ADDRESS("Not indicated address"),
    INVALID_PHONE("Not indicated phone"),
    INVALID_PRODUCT_NAME("Not indicated product name"),
    INVALID_LOGIN_CONTENT("The login must contain only Latin and Russian letters and numbers"),
    INVALID_MIN_LENGTH_PASSWORD("Length of password must be longer then 8"),
    INVALID_MAX_LENGTH_FIRSTNAME("Length of firstname must be less then 50"),
    INVALID_MAX_LENGTH_LASTNAME("Length of lastname must be less then 50"),
    INVALID_MAX_LENGTH_LOGIN("Length of login must be less then 50"),
    INVALID_MAX_LENGTH_PASSWORD("Length of password must be less then 50"),
    INVALID_MAX_LENGTH_POSITION("Length of position must be less then 50"),
    INVALID_MAX_LENGTH_PATRONYMICNAME("Length of patronymycname must be less then 50"),
    INVALID_MAX_LENGTH_EMAIL("Length of email must be less then 50"),
    INVALID_MAX_LENGTH_PHONE("Length of phone must be less then 50"),
    INVALID_MAX_LENGTH_ADDRESS("Length of address must be less then 50"),
    INVALID_MAX_LENGTH_PRODUCT_NAME("Length of product name must be less then 50"),
    INVALID_FIRSTNAME_CONTENT("first name may contain only Russian letters, spaces and a minus sign."),
    INVALID_LASTNAME_CONTENT("last namemay contain only Russian letters, spaces and a minus sign."),
    INVALID_PATRONYMICNAME_CONTENT("patronymic may contain only Russian letters, spaces and a minus sign."),
    INVALID_EMAIL_CONTENT("your email is not valid"),
    NONEXISTENT_CATEGORY("this category doesnt exist"),
    NONEXISTENT_USER("this user doesnt exist"),
    INVALID_USER_TYPE("your user type is not proper"),
    NONEXISTENT_PRODUCT("this product doesnt exist"),
    INVALID_COUNT_PRODUCT("not enough of product"),
    INVALID_DEPOSIT_VALUE("not enough money on deposit"),
    INVALID_NAME_PRODUCT_PURCHASE("name of product in request is different from true value"),
    INVALID_PRICE_PRODUCT_PURCHASE("price of product in request is different from true value"),
    INVALID_CONTENT_PRICE("price of product must be more then 0 or 0"),
    INVALID_CONTENT_PHONE("Your phone is not Russian Federation cell phone"),
    INVALID_CATEGORY_EDIT("Category cannot be changed at SubCategory while editing"),
    INVALID_SUB_CATEGORY_EDIT("Category cannot be changed at SubCategory while editing"),
    INVALID_PURSCHASE_TRY("Product already was purschsed"),
    INVALID_PUT_DEPOSIT_TRY("Deposit is changing from two devices at one time"),
    DATABASE_ERROR("Unexpected error"),
    INVALID_REGISTER_TRY("User with this login already exists");

    private String errorString;

    private ServerErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
