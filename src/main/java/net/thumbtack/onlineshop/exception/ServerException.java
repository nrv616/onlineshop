package net.thumbtack.onlineshop.exception;

public class ServerException extends Exception {
    private ServerErrorCode serverErrorCode;
    private String field;

    public ServerException(ServerErrorCode serverErrorCode, String field) {
        this.serverErrorCode = serverErrorCode;
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public ServerErrorCode getServerErrorCode() {
        return serverErrorCode;
    }
}
