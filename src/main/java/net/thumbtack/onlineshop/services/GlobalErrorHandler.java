package net.thumbtack.onlineshop.services;

import com.google.gson.Gson;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class GlobalErrorHandler {
    private Map<String, ServerErrorCode> serverErrors = new HashMap<>();

    private void mapSet() {
        serverErrors.put("ContainsWordsAndNumbersOnly" + "login", ServerErrorCode.INVALID_LOGIN_CONTENT);
        serverErrors.put("MinSize" + "password", ServerErrorCode.INVALID_MIN_LENGTH_PASSWORD);
        serverErrors.put("NameContent" + "firstName", ServerErrorCode.INVALID_FIRSTNAME_CONTENT);
        serverErrors.put("NameContent" + "lastName", ServerErrorCode.INVALID_LASTNAME_CONTENT);
        serverErrors.put("NameContent" + "patronymicName", ServerErrorCode.INVALID_PATRONYMICNAME_CONTENT);
        serverErrors.put("MaxSize" + "firstName", ServerErrorCode.INVALID_MAX_LENGTH_FIRSTNAME);
        serverErrors.put("MaxSize" + "lastName", ServerErrorCode.INVALID_MAX_LENGTH_LASTNAME);
        serverErrors.put("MaxSize" + "patronymicName", ServerErrorCode.INVALID_MAX_LENGTH_PATRONYMICNAME);
        serverErrors.put("MaxSize" + "login", ServerErrorCode.INVALID_MAX_LENGTH_LOGIN);
        serverErrors.put("MaxSize" + "password", ServerErrorCode.INVALID_MAX_LENGTH_PASSWORD);
        serverErrors.put("MaxSize" + "position", ServerErrorCode.INVALID_MAX_LENGTH_POSITION);
        serverErrors.put("NotNull" + "firstName", ServerErrorCode.INVALID_FIRSTNAME);
        serverErrors.put("NotNull" + "lastName", ServerErrorCode.INVALID_LASTNAME);
        serverErrors.put("NotNull" + "login", ServerErrorCode.INVALID_LOGIN);
        serverErrors.put("NotNull" + "password", ServerErrorCode.INVALID_PASSWORD);
        serverErrors.put("NotNull" + "position", ServerErrorCode.INVALID_POSITION);
        serverErrors.put("NotNull" + "email", ServerErrorCode.INVALID_EMAIL);
        serverErrors.put("NotNull" + "address", ServerErrorCode.INVALID_ADDRESS);
        serverErrors.put("NotNull" + "phone", ServerErrorCode.INVALID_PHONE);
        serverErrors.put("NotNull" + "name", ServerErrorCode.INVALID_PRODUCT_NAME);
        serverErrors.put("NotBlank" + "firstName", ServerErrorCode.INVALID_FIRSTNAME);
        serverErrors.put("NotBlank" + "lastName", ServerErrorCode.INVALID_LASTNAME);
        serverErrors.put("NotBlank" + "login", ServerErrorCode.INVALID_LOGIN);
        serverErrors.put("NotBlank" + "password", ServerErrorCode.INVALID_PASSWORD);
        serverErrors.put("NotBlank" + "position", ServerErrorCode.INVALID_POSITION);
        serverErrors.put("NotBlank" + "email", ServerErrorCode.INVALID_EMAIL);
        serverErrors.put("NotBlank" + "address", ServerErrorCode.INVALID_ADDRESS);
        serverErrors.put("NotBlank" + "phone", ServerErrorCode.INVALID_PHONE);
        serverErrors.put("NotBlank" + "name", ServerErrorCode.INVALID_PRODUCT_NAME);
        serverErrors.put("Email" + "email", ServerErrorCode.INVALID_EMAIL_CONTENT);
        serverErrors.put("MaxSize" + "email", ServerErrorCode.INVALID_MAX_LENGTH_EMAIL);
        serverErrors.put("MaxSize" + "phone", ServerErrorCode.INVALID_MAX_LENGTH_PHONE);
        serverErrors.put("MaxSize" + "address", ServerErrorCode.INVALID_MAX_LENGTH_ADDRESS);
        serverErrors.put("MaxSize" + "name", ServerErrorCode.INVALID_MAX_LENGTH_PRODUCT_NAME);
        serverErrors.put("Phone" + "phone", ServerErrorCode.INVALID_CONTENT_PHONE);
        serverErrors.put("DepositValue" + "deposit", ServerErrorCode.INVALID_DEPOSIT_VALUE);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleValidation(MethodArgumentNotValidException exc) {
        final List<MyError> errors = new ArrayList<>();
        mapSet();
        exc.getBindingResult().getFieldErrors().forEach(fieldError -> {
            MyError error = new MyError();
            error.setField(fieldError.getField());
            ServerErrorCode serverErrorCode = serverErrors.get(fieldError.getCode() + fieldError.getField());
            error.setErrorCode(serverErrorCode.toString());
            error.setMessage(serverErrorCode.getErrorString());
            errors.add(error);
        });
        return new Gson().toJson(errors);
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String allValidation(Exception exc) {
        final List<MyError> errors = new ArrayList<>();
        MyError error = new MyError();
        if (exc.getMessage().contains("Duplicate ")) {
            error.setErrorCode(ServerErrorCode.INVALID_REGISTER_TRY.toString());
            error.setMessage(ServerErrorCode.INVALID_REGISTER_TRY.getErrorString());
            error.setField("login");
        }
        errors.add(error);
        return new Gson().toJson(errors);
    }

    @ExceptionHandler({ServerException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String serverValidation(ServerException exc) {
        final List<MyError> errors = new ArrayList<>();
        MyError error = new MyError();
        error.setErrorCode(exc.getServerErrorCode().toString());
        error.setMessage(exc.getServerErrorCode().getErrorString());
        error.setField(exc.getField());
        errors.add(error);
        return new Gson().toJson(errors);
    }

    public static class MyError {
        private String errorCode;
        private String field;
        private String message;

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
