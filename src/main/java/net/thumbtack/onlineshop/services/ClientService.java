package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditClientProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterClientRequest;
import net.thumbtack.onlineshop.dto.response.GetInfoAboutClientsResponse;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {
    private ClientDaoImpl clientDao;
    private AdminDaoImpl adminDao;

    @Autowired
    public ClientService(ClientDaoImpl clientDao, AdminDaoImpl adminDao) {
        this.clientDao = clientDao;
        this.adminDao = adminDao;
    }

    public RegisterClientResponse registerClient(RegisterClientRequest registerClientRequest, String javaSessionId) throws ServerException {
        Client client = new Client(registerClientRequest.getFirstName(), registerClientRequest.getLastName(), registerClientRequest.getPatronymicName(),
                registerClientRequest.getLogin(), registerClientRequest.getPassword(), registerClientRequest.getEmail(), registerClientRequest.getAddress(),
                registerClientRequest.getPhone().replaceAll("\\D+", ""));
        clientDao.registerClient(client, javaSessionId);
        return new RegisterClientResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone(), 0);
    }

    public List<GetInfoAboutClientsResponse> getClients(String javaSessionId) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Client> clients = clientDao.getClients();
        List<GetInfoAboutClientsResponse> result = new ArrayList<>();
        for (Client client : clients) {
            result.add(new GetInfoAboutClientsResponse(client.getId(), client.getFirstName(),
                    client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone()));
        }
        return result;
    }

    public RegisterClientResponse editClientProfile(String javaSessionId, EditClientProfileRequest editClientProfileRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        if (editClientProfileRequest.getPatronymicName() == null) {
            editClientProfileRequest.setPatronymicName("");
        }
        client = new Client(client.getId(), editClientProfileRequest.getFirstName(), editClientProfileRequest.getLastName(), editClientProfileRequest.getPatronymicName(), editClientProfileRequest.getNewPassword(), editClientProfileRequest.getEmail(), editClientProfileRequest.getAddress(), editClientProfileRequest.getPhone());
        clientDao.editClientProfile(client);
        return new RegisterClientResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone(), 0);
    }
}