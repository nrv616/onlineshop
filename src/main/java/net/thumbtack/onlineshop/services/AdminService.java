package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.UserDaoImpl;
import net.thumbtack.onlineshop.dto.request.EditAdminProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    private AdminDaoImpl adminDao;
    private UserDaoImpl userDao;

    @Autowired
    public AdminService(AdminDaoImpl adminDao, UserDaoImpl userDao) {
        this.adminDao = adminDao;
        this.userDao = userDao;
    }

    public RegisterAdminResponse registerAdmin(RegisterAdminRequest registerAdminRequest, String javaSessionId) throws ServerException {
        Admin admin = new Admin(registerAdminRequest.getFirstName(), registerAdminRequest.getLastName(), registerAdminRequest.getPatronymicName(), registerAdminRequest.getLogin(), registerAdminRequest.getPassword(), registerAdminRequest.getPosition());
        adminDao.registerAdmin(admin, javaSessionId);
        return new RegisterAdminResponse(admin.getId(), admin.getFirstName(), admin.getLastName(), admin.getPatronymicName(), admin.getPosition());
    }

    public RegisterAdminResponse editAdminProfile(String javaSessionId, EditAdminProfileRequest editAdminProfileRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        User user = userDao.getUserByCookie(javaSessionId);
        if (user == null) {
            throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "id");
        }
        Admin admin = adminDao.getAdmin(user.getId());
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        if (editAdminProfileRequest.getPatronymicName() == null) {
            editAdminProfileRequest.setPatronymicName("");
        }
        admin = new Admin(admin.getId(), editAdminProfileRequest.getFirstName(), editAdminProfileRequest.getLastName(), editAdminProfileRequest.getPatronymicName(), editAdminProfileRequest.getNewPassword(), editAdminProfileRequest.getPosition());
        adminDao.editAdminProfile(admin);
        return new RegisterAdminResponse(admin.getId(), admin.getFirstName(), admin.getLastName(), admin.getPatronymicName(), admin.getPosition());
    }
}