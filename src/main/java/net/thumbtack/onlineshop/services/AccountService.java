package net.thumbtack.onlineshop.services;

import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.AccountDaoImpl;
import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    private AccountDaoImpl accountDao;
    private AdminDaoImpl adminDao;
    private ClientDaoImpl clientDao;

    @Autowired
    public AccountService(AccountDaoImpl accountDao, AdminDaoImpl adminDao, ClientDaoImpl clientDao) {
        this.accountDao = accountDao;
        this.adminDao = adminDao;
        this.clientDao = clientDao;
    }

    public String getUserInfo(String javaSessionId) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        User user = accountDao.getUserInfo(javaSessionId);
        if (user == null) {
            throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "id");
        }
        Admin admin = adminDao.getAdmin(user.getId());
        Client client = clientDao.getClient(user.getId());
        if (client == null) {
            return new Gson().toJson(new RegisterAdminResponse(admin.getId(), admin.getFirstName(), admin.getLastName(), admin.getPatronymicName(), admin.getPosition()));
        } else {
            return new Gson().toJson(new RegisterClientResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone(), client.getDeposit()));
        }
    }
}
