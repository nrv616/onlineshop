package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.CategoryDaoImpl;
import net.thumbtack.onlineshop.dto.request.AddCategoryRequest;
import net.thumbtack.onlineshop.dto.request.EditCategoryRequest;
import net.thumbtack.onlineshop.dto.response.AddCategoryResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CategoryService {
    private CategoryDaoImpl categoryDao;
    private AdminDaoImpl adminDao;

    @Autowired
    public CategoryService(CategoryDaoImpl categoryDao, AdminDaoImpl adminDao) {
        this.categoryDao = categoryDao;
        this.adminDao = adminDao;
    }

    public AddCategoryResponse addCategory(String javaSessionId, AddCategoryRequest addCategoryRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        Category category;
        if (addCategoryRequest.getParentId() == 0) {
            category = categoryDao.addCategory(new Category(addCategoryRequest.getName()));
        } else {
            category = categoryDao.addSubCategory(new Category(0, addCategoryRequest.getName(), addCategoryRequest.getParentId()));
        }
        if (category.getParent().getName() == null) {
            return new AddCategoryResponse(category.getId(), category.getName());
        } else {
            return new AddCategoryResponse(category.getId(), category.getName(), category.getParent().getId(), category.getParent().getName());
        }
    }

    public AddCategoryResponse getCategoryById(String javaSessionId, int id) throws ServerException {
        Category category = categoryDao.getCategoryById(id);
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        if (category.getParent().getName() == null) {
            return new AddCategoryResponse(category.getId(), category.getName());
        } else {
            return new AddCategoryResponse(category.getId(), category.getName(), category.getParent().getId(), category.getParent().getName());
        }
    }

    public AddCategoryResponse editCategoryById(String javaSessionId, int id, EditCategoryRequest editCategoryRequest) throws ServerException {
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        Category category1 = categoryDao.getCategoryById(id);
        if (editCategoryRequest.getName() == null || editCategoryRequest.getName().equals("")) {
            editCategoryRequest.setName(category1.getName());
        }
        if (editCategoryRequest.getParentId() == null) {
            editCategoryRequest.setParentId(category1.getParentId());
        }
        if (editCategoryRequest.getParentId() != 0 && category1.getParentId() == 0) {
            throw new ServerException(ServerErrorCode.INVALID_CATEGORY_EDIT, "parent");
        }
        if (editCategoryRequest.getParentId() == 0 && category1.getParentId() != 0) {
            throw new ServerException(ServerErrorCode.INVALID_SUB_CATEGORY_EDIT, "parent");
        }
        Category category = categoryDao.editCategoryById(id, editCategoryRequest.getName(), editCategoryRequest.getParentId());
        if (category.getParent().getName() == null) {
            return new AddCategoryResponse(category.getId(), category.getName());
        } else {
            return new AddCategoryResponse(category.getId(), category.getName(), category.getParent().getId(), category.getParent().getName());
        }
    }

    public void deleteCategoryById(String javaSessionId, int id) throws ServerException {
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        categoryDao.deleteCategoryById(id);
    }

    public List<AddCategoryResponse> getCategories(String javaSessionId) throws ServerException {
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Category> categories = categoryDao.getCategories();
        Map<Integer, List<Category>> map = categories.stream().collect(Collectors.groupingBy(Category::getParentId));
        List<AddCategoryResponse> result = new ArrayList<>();
        for (Category tempCategory1 : map.get(0)) {
            List<Category> temp = map.get(tempCategory1.getId());
            result.add(new AddCategoryResponse(tempCategory1.getId(), tempCategory1.getName()));
            if (temp != null) {
                for (Category tempCategory : temp) {
                    result.add(new AddCategoryResponse(tempCategory.getId(), tempCategory.getName(), tempCategory.getParentId(), tempCategory.getParent().getName()));
                }
            }
        }
        return result;
    }
}
