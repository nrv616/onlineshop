package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.dto.response.GetSettingsResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SettingsService {
    @Value("${max_name_length}")
    private int maxNameLength;
    @Value("${min_password_length}")
    private int minPasswordLength;

    public GetSettingsResponse getSettings(String javaSessionId) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        return new GetSettingsResponse(maxNameLength, minPasswordLength);
    }
}
