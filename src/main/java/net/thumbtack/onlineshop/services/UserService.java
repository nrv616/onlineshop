package net.thumbtack.onlineshop.services;

import com.google.gson.Gson;
import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.UserDaoImpl;
import net.thumbtack.onlineshop.dto.request.LoginUserRequest;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private UserDaoImpl userDao;
    private AdminDaoImpl adminDao;
    private ClientDaoImpl clientDao;

    @Autowired
    public UserService(UserDaoImpl userDao, AdminDaoImpl adminDao, ClientDaoImpl clientDao) {
        this.userDao = userDao;
        this.adminDao = adminDao;
        this.clientDao = clientDao;
    }

    public String loginUser(LoginUserRequest loginUserRequest, String javaSessionId) throws ServerException {
        User user = userDao.logIn(loginUserRequest.getLogin(), loginUserRequest.getPassword(), javaSessionId);
        Admin admin = adminDao.getAdmin(user.getId());
        Client client = clientDao.getClient(user.getId());
        if (client == null) {
            return new Gson().toJson(new RegisterAdminResponse(admin.getId(), admin.getFirstName(), admin.getLastName(), admin.getPatronymicName(), admin.getPosition()));
        } else {
            return new Gson().toJson(new RegisterClientResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone(), client.getDeposit()));
        }
    }

    public void logout(String javaSessionId) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        userDao.logOut(javaSessionId);
    }
}
