package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.BasketDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.dto.response.PurchaseResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BasketService {
    private BasketDaoImpl basketDao;
    private ClientDaoImpl clientDao;

    @Autowired
    public BasketService(BasketDaoImpl basketDao, ClientDaoImpl clientDao) {
        this.basketDao = basketDao;
        this.clientDao = clientDao;
    }

    public List<PurchaseResponse> addProductToBasket(String javaSessionId, PurchaseRequest purchaseRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        Product productTemp = basketDao.getProductById(purchaseRequest.getId());
        if (!purchaseRequest.getName().equals(productTemp.getName())) {
            throw new ServerException(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE, "name");
        }
        if (purchaseRequest.getPrice() != productTemp.getPrice()) {
            throw new ServerException(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE, "price");
        }
        int basketId = basketDao.addProductToBasket(client, purchaseRequest.getId(), purchaseRequest.getName(), purchaseRequest.getPrice(), purchaseRequest.getCount());
        List<Product> products = basketDao.getProductsFromBasketById(basketId);
        List<PurchaseResponse> result = new ArrayList<>();
        for (Product product : products) {
            result.add(new PurchaseResponse(product.getId(), product.getName(), product.getPrice(), product.getCount()));
        }
        return result;
    }

    public void deleteProduct(String javaSessionId, int id) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        basketDao.deleteProduct(client, id);
    }

    public List<PurchaseResponse> editProductFromBasket(String javaSessionId, PurchaseRequest purchaseRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        Product productTemp = basketDao.getProductById(purchaseRequest.getId());
        if (!purchaseRequest.getName().equals(productTemp.getName())) {
            throw new ServerException(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE, "name");
        }
        if (purchaseRequest.getPrice() != productTemp.getPrice()) {
            throw new ServerException(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE, "price");
        }
        int basketId = basketDao.editProductFromBasket(client, purchaseRequest.getId(), purchaseRequest.getName(), purchaseRequest.getPrice(), purchaseRequest.getCount());
        List<Product> products = basketDao.getProductsFromBasketById(basketId);
        List<PurchaseResponse> result = new ArrayList<>();
        for (Product product : products) {
            result.add(new PurchaseResponse(product.getId(), product.getName(), product.getPrice(), product.getCount()));
        }
        return result;
    }

    public List<PurchaseResponse> getProductsFromBasket(String javaSessionId) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Product> products = basketDao.getProductsFromBasket(client);
        List<PurchaseResponse> result = new ArrayList<>();
        for (Product product : products) {
            result.add(new PurchaseResponse(product.getId(), product.getName(), product.getPrice(), product.getCount()));
        }
        return result;
    }
}
