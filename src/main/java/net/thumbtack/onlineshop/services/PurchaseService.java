package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.BasketDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.PurchaseDaoImpl;
import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.dto.response.GetClientsBuyingProductResponse;
import net.thumbtack.onlineshop.dto.response.GetPurchasesResponse;
import net.thumbtack.onlineshop.dto.response.PurchaseBasketResponse;
import net.thumbtack.onlineshop.dto.response.PurchaseResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class PurchaseService {
    private PurchaseDaoImpl purchaseDao;
    private BasketDaoImpl basketDao;
    private ClientDaoImpl clientDao;
    private AdminDaoImpl adminDao;


    @Autowired
    public PurchaseService(PurchaseDaoImpl purchaseDao, BasketDaoImpl basketDao, ClientDaoImpl clientDao, AdminDaoImpl adminDao) {
        this.purchaseDao = purchaseDao;
        this.basketDao = basketDao;
        this.clientDao = clientDao;
        this.adminDao = adminDao;
    }

    public PurchaseResponse purchase(String javaSessionId, PurchaseRequest purchaseRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        Product product = basketDao.getProductById(purchaseRequest.getId());
        if (product.getCount() < purchaseRequest.getCount()) {
            throw new ServerException(ServerErrorCode.INVALID_COUNT_PRODUCT, "count");
        }
        if (!purchaseRequest.getName().equals(product.getName())) {
            throw new ServerException(ServerErrorCode.INVALID_NAME_PRODUCT_PURCHASE, "name");
        }
        if (purchaseRequest.getPrice() != product.getPrice()) {
            throw new ServerException(ServerErrorCode.INVALID_PRICE_PRODUCT_PURCHASE, "price");
        }
        int deposit = purchaseDao.getDeposit(javaSessionId);
        int newDeposit = deposit - purchaseRequest.getCount() * product.getPrice();
        if (newDeposit < 0) {
            throw new ServerException(ServerErrorCode.INVALID_DEPOSIT_VALUE, "deposit");
        }
        int newCount = product.getCount() - purchaseRequest.getCount();
        purchaseDao.purchase(client, purchaseRequest.getId(), purchaseRequest.getName(), purchaseRequest.getPrice(), purchaseRequest.getCount(), newDeposit, newCount);
        return new PurchaseResponse(product.getId(), product.getName(), product.getPrice(), newCount);
    }

    public PurchaseBasketResponse purchaseBasket(String javaSessionId, PurchaseRequest purchaseRequest[]) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Product> request = new ArrayList<>();
        for (int i = 0; i < purchaseRequest.length; i++) {
            request.add(new Product(purchaseRequest[i].getId(), purchaseRequest[i].getName(), purchaseRequest[i].getPrice(), purchaseRequest[i].getCount()));
        }
        List<Product> bought = purchaseDao.purchaseBasket(client, request);
        List<Product> remaining = basketDao.getProductsFromBasket(client);
        PurchaseBasketResponse purchaseBasketResponse = new PurchaseBasketResponse();
        for (Product product : bought) {
            purchaseBasketResponse.addBought(new PurchaseResponse(product.getId(), product.getName(), product.getPrice(), product.getCount()));
        }
        for (Product product : remaining) {
            purchaseBasketResponse.addRemaining(new PurchaseResponse(product.getId(), product.getName(), product.getPrice(), product.getCount()));
        }
        return purchaseBasketResponse;
    }

    public List<GetPurchasesResponse> getPurchasesOfClient(String javaSessionId, int id, String order, Integer offset, Integer limit) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Product> products;
        if (limit == null) {
            products = purchaseDao.getPurchasesOfClient(id);
        } else {
            products = purchaseDao.getPurchasesOfClientLimit(id, offset, limit);
        }
        if (order.equals("name")) {
            products.sort(Comparator.comparing(Product::getName));
        }
        if (order.equals("price")) {
            products.sort(Comparator.comparing(Product::getPrice));
        }
        List<GetPurchasesResponse> result = new ArrayList<>();
        for (Product product : products) {
            result.add(new GetPurchasesResponse(product.getId(), product.getName(), product.getPrice(), product.getCount()));
        }
        return result;
    }

    public List<GetPurchasesResponse> getPurchasesOfClients(String javaSessionId, List<Integer> clientsId, String order, Integer offset, Integer limit) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Product> products;
        if (limit == null) {
            products = purchaseDao.getPurchasesOfClients(clientsId);
        } else {
            products = purchaseDao.getPurchasesOfClientsLimit(clientsId, offset, limit);
        }
        if (order.equals("name")) {
            products.sort(Comparator.comparing(Product::getName));
        }
        if (order.equals("price")) {
            products.sort(Comparator.comparing(Product::getPrice));
        }
        List<GetPurchasesResponse> result = new ArrayList<>();
        for (Product product : products) {
            result.add(new GetPurchasesResponse(product.getId(), product.getName(), product.getPrice(), product.getCount()));
        }
        return result;
    }

    public List<GetClientsBuyingProductResponse> getClientsBuyingProduct(String javaSessionId, int id, Integer offset, Integer limit) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Client> clients;
        if (limit == null) {
            clients = purchaseDao.getClientsBuyingProduct(id);
        } else {
            clients = purchaseDao.getClientsBuyingProductLimit(id, offset, limit);
        }
        List<GetClientsBuyingProductResponse> result = new ArrayList<>();
        for (Client client : clients) {
            result.add(new GetClientsBuyingProductResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone()));
        }
        return result;
    }

    public List<GetClientsBuyingProductResponse> getClientsBuyingProducts(String javaSessionId, List<Integer> productsId, Integer offset, Integer limit) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        List<Client> clients;
        if (limit == null) {
            clients = purchaseDao.getClientsBuyingProducts(productsId);
        } else {
            clients = purchaseDao.getClientsBuyingProductsLimit(productsId, offset, limit);
        }
        List<GetClientsBuyingProductResponse> result = new ArrayList<>();
        for (Client client : clients) {
            result.add(new GetClientsBuyingProductResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone()));
        }
        return result;
    }
}
