package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.ClientDaoImpl;
import net.thumbtack.onlineshop.daoimpl.DepositDaoImpl;
import net.thumbtack.onlineshop.dto.request.PutDepositRequest;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepositService {
    private DepositDaoImpl depositDao;
    private ClientDaoImpl clientDao;

    @Autowired
    public DepositService(DepositDaoImpl depositDao, ClientDaoImpl clientDao) {
        this.depositDao = depositDao;
        this.clientDao = clientDao;
    }

    public RegisterClientResponse putDeposit(String javaSessionId, PutDepositRequest putDepositRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        depositDao.putDeposit(client, putDepositRequest.getDeposit());
        return new RegisterClientResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone(), client.getDeposit() + putDepositRequest.getDeposit());
    }

    public RegisterClientResponse getDeposit(String javaSessionId) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Client client = clientDao.getClientByCookie(javaSessionId);
        if (client == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        return new RegisterClientResponse(client.getId(), client.getFirstName(), client.getLastName(), client.getPatronymicName(), client.getEmail(), client.getAddress(), client.getPhone(), client.getDeposit());
    }
}
