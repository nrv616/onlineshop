package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.AdminDaoImpl;
import net.thumbtack.onlineshop.daoimpl.ProductDaoImpl;
import net.thumbtack.onlineshop.daoimpl.UserDaoImpl;
import net.thumbtack.onlineshop.dto.request.AddProductRequest;
import net.thumbtack.onlineshop.dto.request.EditProductRequest;
import net.thumbtack.onlineshop.dto.response.AddProductResponse;
import net.thumbtack.onlineshop.dto.response.GetInfoProductResponse;
import net.thumbtack.onlineshop.exception.ServerErrorCode;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.Product;
import net.thumbtack.onlineshop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    private ProductDaoImpl productDao;
    private AdminDaoImpl adminDao;
    private UserDaoImpl userDao;

    @Autowired
    public ProductService(ProductDaoImpl productDao, AdminDaoImpl adminDao, UserDaoImpl userDao) {
        this.productDao = productDao;
        this.adminDao = adminDao;
        this.userDao = userDao;
    }

    public AddProductResponse addProduct(String javaSessionId, AddProductRequest addProductRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        if (addProductRequest.getPrice() < 0) {
            throw new ServerException(ServerErrorCode.INVALID_CONTENT_PRICE, "price");
        }
        Product product = productDao.addProduct(new Product(addProductRequest.getName(), addProductRequest.getPrice(), addProductRequest.getCount()));
        if (addProductRequest.getCategories() != null) {
            productDao.addProductToCategories(addProductRequest.getCategories(), product.getId());
        }
        return new AddProductResponse(product.getId(), product.getName(), product.getPrice(), product.getCount(), addProductRequest.getCategories());
    }

    public AddProductResponse editProduct(String javaSessionId, int id, EditProductRequest editProductRequest) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        Product product = productDao.getInfoProduct(id);
        if (editProductRequest.getName() != null) {
            product.setName(editProductRequest.getName());
        }
        if (editProductRequest.getPrice() != null) {
            product.setPrice(editProductRequest.getPrice());
        }
        if (editProductRequest.getCount() != null) {
            product.setCount(editProductRequest.getCount());
        }
        productDao.editProduct(product, editProductRequest.getCategories());
        List<Integer> categoriesId = new ArrayList<>();
        for (Category category : product.getCategories()) {
            categoriesId.add(category.getId());
        }
        return new AddProductResponse(product.getId(), product.getName(), product.getPrice(), product.getCount(), categoriesId);
    }

    public void deleteProduct(String javaSessionId, int id) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        Admin admin = adminDao.getAdminByCookie(javaSessionId);
        if (admin == null) {
            throw new ServerException(ServerErrorCode.INVALID_USER_TYPE, "id");
        }
        productDao.deleteProduct(id);
    }

    public GetInfoProductResponse getInfoProduct(String javaSessionId, int id) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        User user = userDao.getUserByCookie(javaSessionId);
        if (user == null) {
            throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "id");
        }
        Product product = productDao.getInfoProduct(id);
        List<String> categoriesNames = new ArrayList<>();
        for (Category category : product.getCategories()) {
            categoriesNames.add(category.getName());
        }
        return new GetInfoProductResponse(product.getId(), product.getName(), product.getCount(), product.getPrice(), categoriesNames);
    }

    public List<GetInfoProductResponse> getProductsByCategories(String javaSessionId, String order, List<Integer> categories) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        User user = userDao.getUserByCookie(javaSessionId);
        if (user == null) {
            throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "id");
        }
        List<Product> products;
        if (order.equals("product")) {
            products = productDao.getProductsByCategoriesOrderProduct(categories);
        } else {
            products = productDao.getProductsByCategoriesOrderCategory(categories);
        }
        List<GetInfoProductResponse> result = new ArrayList<>();
        for (Product product : products) {
            List<String> categoryNames = new ArrayList<>();
            for (Category category : product.getCategories()) {
                categoryNames.add(category.getName());
            }
            result.add(new GetInfoProductResponse(product.getId(), product.getName(), product.getCount(), product.getPrice(), categoryNames));
        }
        return result;
    }

    public List<GetInfoProductResponse> getProducts(String javaSessionId, String order) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        User user = userDao.getUserByCookie(javaSessionId);
        if (user == null) {
            throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "id");
        }
        List<Product> products;
        if (order.equals("product")) {
            products = productDao.getProducts();
        } else {
            products = productDao.getProductsOrderCategory();
        }
        List<GetInfoProductResponse> result = new ArrayList<>();
        for (Product product : products) {
            List<String> categoryNames = new ArrayList<>();
            for (Category category : product.getCategories()) {
                categoryNames.add(category.getName());
            }
            result.add(new GetInfoProductResponse(product.getId(), product.getName(), product.getCount(), product.getPrice(), categoryNames));
        }
        return result;
    }

    public List<GetInfoProductResponse> getProductsWithoutCategories(String javaSessionId, String order) throws ServerException {
        if (javaSessionId == null) {
            throw new ServerException(ServerErrorCode.INVALID_JAVASESSIONID, "javaSessionId");
        }
        User user = userDao.getUserByCookie(javaSessionId);
        if (user == null) {
            throw new ServerException(ServerErrorCode.NONEXISTENT_USER, "id");
        }
        List<Product> products = productDao.getProductsWithoutCategories(order);
        List<GetInfoProductResponse> result = new ArrayList<>();
        for (Product product : products) {
            result.add(new GetInfoProductResponse(product.getId(), product.getName(), product.getCount(), product.getPrice(), new ArrayList<>()));
        }
        return result;
    }
}
