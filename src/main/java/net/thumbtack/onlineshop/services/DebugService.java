package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.daoimpl.CommonDaoImpl;
import net.thumbtack.onlineshop.exception.ServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DebugService {
    private CommonDaoImpl commonDao;

    @Autowired
    public DebugService(CommonDaoImpl commonDao) {
        this.commonDao = commonDao;
    }

    public void clear() throws ServerException {
        commonDao.clear();
    }
}
