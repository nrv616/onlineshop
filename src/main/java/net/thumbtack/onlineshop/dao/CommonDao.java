package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;

public interface CommonDao {
    void clear() throws ServerException;
}
