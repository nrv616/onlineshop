package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;

public interface DepositDao {
    void putDeposit(Client client, int deposit) throws ServerException;
}
