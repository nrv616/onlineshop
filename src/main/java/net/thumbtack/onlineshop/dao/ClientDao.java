package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;

import java.util.List;

public interface ClientDao {
    void registerClient(Client client, String javaSessionId) throws ServerException;

    void editClientProfile(Client client) throws ServerException;

    List<Client> getClients() throws ServerException;

    Client getClient(int id) throws ServerException;

    Client getClientByCookie(String cookie) throws ServerException;
}
