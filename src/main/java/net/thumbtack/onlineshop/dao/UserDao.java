package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.User;

public interface UserDao {
    User logIn(String login, String password, String javaSessionId) throws ServerException;

    void logOut(String cookie) throws ServerException;

    User getUserByCookie(String cookie) throws ServerException;
}
