package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.User;

public interface AccountDao {
    User getUserInfo(String cookies) throws ServerException;
}
