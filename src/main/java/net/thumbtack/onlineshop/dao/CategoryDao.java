package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Category;

import java.util.List;

public interface CategoryDao {
    Category addCategory(Category category) throws ServerException;

    Category getCategoryById(int id) throws ServerException;

    Category editCategoryById(int id, String name, int parentId) throws ServerException;

    void deleteCategoryById(int id) throws ServerException;

    List<Category> getCategories() throws ServerException;

    Category addSubCategory(Category category) throws ServerException;
}
