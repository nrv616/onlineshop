package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Admin;

public interface AdminDao {
    void registerAdmin(Admin admin, String javaSessionId) throws ServerException;

    void editAdminProfile(Admin admin) throws ServerException;

    Admin getAdmin(int id) throws ServerException;

    Admin getAdminByCookie(String cookie) throws ServerException;
}
