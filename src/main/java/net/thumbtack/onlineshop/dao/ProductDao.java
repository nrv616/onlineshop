package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Product;

import java.util.List;

public interface ProductDao {
    Product addProduct(Product product) throws ServerException;

    void addProductToCategories(List<Integer> categories, int id) throws ServerException;

    void editProduct(Product product, List<Integer> categories) throws ServerException;

    void deleteProduct(int id) throws ServerException;

    Product getInfoProduct(int id) throws ServerException;

    List<Product> getProductsByCategoriesOrderProduct(List<Integer> categories) throws ServerException;

    List<Product> getProductsByCategoriesOrderCategory(List<Integer> categories) throws ServerException;

    List<Product> getProducts() throws ServerException;

    List<Product> getProductsOrderCategory() throws ServerException;

    List<Product> getProductsWithoutCategories(String order) throws ServerException;
}
