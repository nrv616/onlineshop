package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.Product;

import java.util.List;

public interface BasketDao {
    int addProductToBasket(Client client, int id, String name, int price, int count) throws ServerException;

    void deleteProduct(Client client, int id) throws ServerException;

    int editProductFromBasket(Client client, int id, String name, int price, int count) throws ServerException;

    List<Product> getProductsFromBasket(Client client) throws ServerException;

    List<Product> getProductsFromBasketById(int id) throws ServerException;

    Product getProductById(int id) throws ServerException;
}
