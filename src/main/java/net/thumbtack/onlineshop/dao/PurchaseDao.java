package net.thumbtack.onlineshop.dao;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.Product;

import java.util.List;

public interface PurchaseDao {
    void purchase(Client client, int id, String name, int price, int count, int newDeposit, int newCount) throws ServerException;

    List<Product> purchaseBasket(Client client, List<Product> products) throws ServerException;

    List<Product> getPurchasesOfClient(int id) throws ServerException;

    List<Product> getPurchasesOfClientLimit(int id, int offset, int limit) throws ServerException;

    List<Product> getPurchasesOfClients(List<Integer> clientsId) throws ServerException;

    List<Product> getPurchasesOfClientsLimit(List<Integer> clientsId, int offset, int limit) throws ServerException;

    List<Client> getClientsBuyingProduct(int id) throws ServerException;

    List<Client> getClientsBuyingProductLimit(int id, int offset, int limit) throws ServerException;

    List<Client> getClientsBuyingProducts(List<Integer> productsId) throws ServerException;

    List<Client> getClientsBuyingProductsLimit(List<Integer> productsId, int offset, int limit) throws ServerException;

    int getDeposit(String javaSessionId) throws ServerException;
}
