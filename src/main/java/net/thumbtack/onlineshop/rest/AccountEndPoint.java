package net.thumbtack.onlineshop.rest;

import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/accounts")
public class AccountEndPoint {
    private AccountService accountService;

    @Autowired
    public AccountEndPoint(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getUserInfo(HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(accountService.getUserInfo(cookieValue), HttpStatus.OK);
    }
}
