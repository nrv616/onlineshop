package net.thumbtack.onlineshop.rest;

import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.dto.response.GetClientsBuyingProductResponse;
import net.thumbtack.onlineshop.dto.response.GetPurchasesResponse;
import net.thumbtack.onlineshop.dto.response.PurchaseBasketResponse;
import net.thumbtack.onlineshop.dto.response.PurchaseResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/purchases")
public class PurchaseEndPoint {
    private PurchaseService purchaseService;

    @Autowired
    public PurchaseEndPoint(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }


    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PurchaseResponse> purchase(@Valid @RequestBody PurchaseRequest purchaseRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(purchaseService.purchase(cookieValue, purchaseRequest), HttpStatus.OK);
    }

    @PostMapping(value = "/baskets", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PurchaseBasketResponse> purchaseBasket(@Valid @RequestBody PurchaseRequest purchaseRequest[], HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(purchaseService.purchaseBasket(cookieValue, purchaseRequest), HttpStatus.OK);
    }

    @GetMapping(value = "/clients/{numOfClient}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GetPurchasesResponse>> getPurchasesOfClient(@PathVariable("numOfClient") int numOfClient,
                                                                           @RequestParam(value = "client", required = false) List<Integer> client,
                                                                           @RequestParam(value = "order", required = false, defaultValue = "name") String order,
                                                                           @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                                                                           @RequestParam(value = "limit", required = false) Integer limit, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        if (client != null) {
            client.add(numOfClient);
            return new ResponseEntity<>(purchaseService.getPurchasesOfClients(cookieValue, client, order, offset, limit), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(purchaseService.getPurchasesOfClient(cookieValue, numOfClient, order, offset, limit), HttpStatus.OK);
        }
    }

    @GetMapping(value = "/products/{numOfProduct}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GetClientsBuyingProductResponse>> getClientsBuyingProducts(@PathVariable("numOfProduct") int numOfProduct,
                                                                                          @RequestParam(value = "product", required = false) List<Integer> product,
                                                                                          @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                                                                                          @RequestParam(value = "limit", required = false) Integer limit, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        if (product != null) {
            product.add(numOfProduct);
            return new ResponseEntity<>(purchaseService.getClientsBuyingProducts(cookieValue, product, offset, limit), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(purchaseService.getClientsBuyingProduct(cookieValue, numOfProduct, offset, limit), HttpStatus.OK);
        }
    }
}
