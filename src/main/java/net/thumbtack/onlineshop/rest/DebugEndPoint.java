package net.thumbtack.onlineshop.rest;

import com.google.gson.Gson;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.DebugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/debug")
public class DebugEndPoint {
    private DebugService debugService;

    @Autowired
    public DebugEndPoint(DebugService debugService) {
        this.debugService = debugService;
    }

    @PostMapping(value = "/clear")
    public ResponseEntity<String> clear() throws ServerException {
        debugService.clear();
        return new ResponseEntity<>(new Gson().toJson(""), HttpStatus.OK);
    }
}
