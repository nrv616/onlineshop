package net.thumbtack.onlineshop.rest;

import com.google.gson.Gson;
import net.thumbtack.onlineshop.dto.request.AddCategoryRequest;
import net.thumbtack.onlineshop.dto.request.EditCategoryRequest;
import net.thumbtack.onlineshop.dto.response.AddCategoryResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryEndPoint {
    private CategoryService categoryService;

    @Autowired
    public CategoryEndPoint(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddCategoryResponse> addCategory(@Valid @RequestBody AddCategoryRequest addCategoryRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(categoryService.addCategory(cookieValue, addCategoryRequest), HttpStatus.OK);
    }

    @GetMapping(value = "/{numOfCategory}")
    public ResponseEntity<AddCategoryResponse> getCategoryById(@PathVariable("numOfCategory") int numOfCategory, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(categoryService.getCategoryById(cookieValue, numOfCategory), HttpStatus.OK);
    }

    @PutMapping(value = "/{numOfCategory}")
    public ResponseEntity<AddCategoryResponse> editCategoryById(@PathVariable("numOfCategory") int numOfCategory, @Valid @RequestBody EditCategoryRequest editCategoryRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(categoryService.editCategoryById(cookieValue, numOfCategory, editCategoryRequest), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{numOfCategory}")
    public ResponseEntity<String> deleteCategoryById(@PathVariable("numOfCategory") int numOfCategory, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        categoryService.deleteCategoryById(cookieValue, numOfCategory);
        return new ResponseEntity<>(new Gson().toJson(""), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AddCategoryResponse>> getCategories(HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(categoryService.getCategories(cookieValue), HttpStatus.OK);
    }
}
