package net.thumbtack.onlineshop.rest;

import net.thumbtack.onlineshop.dto.request.PutDepositRequest;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/deposits")
public class DepositEndPoint {
    private DepositService depositService;

    @Autowired
    public DepositEndPoint(DepositService depositService) {
        this.depositService = depositService;
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterClientResponse> putDeposit(@Valid @RequestBody PutDepositRequest putDepositRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(depositService.putDeposit(cookieValue, putDepositRequest), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterClientResponse> getDeposit(HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(depositService.getDeposit(cookieValue), HttpStatus.OK);
    }
}
