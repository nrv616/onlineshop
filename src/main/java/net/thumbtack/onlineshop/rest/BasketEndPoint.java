package net.thumbtack.onlineshop.rest;

import com.google.gson.Gson;
import net.thumbtack.onlineshop.dto.request.PurchaseRequest;
import net.thumbtack.onlineshop.dto.response.PurchaseResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/baskets")
public class BasketEndPoint {
    private BasketService basketService;

    @Autowired
    public BasketEndPoint(BasketService basketService) {
        this.basketService = basketService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PurchaseResponse>> addProductToBasket(@Valid @RequestBody PurchaseRequest purchaseRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(basketService.addProductToBasket(cookieValue, purchaseRequest), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{numOfProduct}")
    public ResponseEntity<String> deleteProduct(@PathVariable("numOfProduct") int numOfProduct, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        basketService.deleteProduct(cookieValue, numOfProduct);
        return new ResponseEntity<>(new Gson().toJson(""), HttpStatus.OK);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PurchaseResponse>> editProductFromBasket(@Valid @RequestBody PurchaseRequest purchaseRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(basketService.editProductFromBasket(cookieValue, purchaseRequest), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<PurchaseResponse>> getProductsFromBasket(HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(basketService.getProductsFromBasket(cookieValue), HttpStatus.OK);
    }
}
