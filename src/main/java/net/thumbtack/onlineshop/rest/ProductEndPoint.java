package net.thumbtack.onlineshop.rest;

import net.thumbtack.onlineshop.dto.request.AddProductRequest;
import net.thumbtack.onlineshop.dto.request.EditProductRequest;
import net.thumbtack.onlineshop.dto.response.AddProductResponse;
import net.thumbtack.onlineshop.dto.response.GetInfoProductResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductEndPoint {
    private ProductService productService;

    @Autowired
    public ProductEndPoint(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddProductResponse> addProduct(@Valid @RequestBody AddProductRequest addProductRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(productService.addProduct(cookieValue, addProductRequest), HttpStatus.OK);
    }

    @PutMapping(value = "/{numOfProduct}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddProductResponse> editProduct(@PathVariable("numOfProduct") int numOfProduct, @Valid @RequestBody EditProductRequest editProductRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(productService.editProduct(cookieValue, numOfProduct, editProductRequest), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{numOfProduct}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteProduct(@PathVariable("numOfProduct") int numOfProduct, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        productService.deleteProduct(cookieValue, numOfProduct);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @GetMapping(value = "/{numOfProduct}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetInfoProductResponse> getInfoProduct(@PathVariable("numOfProduct") int numOfProduct, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(productService.getInfoProduct(cookieValue, numOfProduct), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GetInfoProductResponse>> getProducts(@RequestParam(value = "category", required = false) List<Integer> category,
                                                                    @RequestParam(value = "order", required = false, defaultValue = "product") String order, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        if (category == null) {
            return new ResponseEntity<>(productService.getProducts(cookieValue, order), HttpStatus.OK);
        } else {
            if (category.equals(Collections.EMPTY_LIST)) {
                return new ResponseEntity<>(productService.getProductsWithoutCategories(cookieValue, order), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(productService.getProductsByCategories(cookieValue, order, category), HttpStatus.OK);
            }
        }
    }

}
