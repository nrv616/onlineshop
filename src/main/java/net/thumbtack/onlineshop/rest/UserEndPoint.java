package net.thumbtack.onlineshop.rest;

import com.google.gson.Gson;
import net.thumbtack.onlineshop.dto.request.LoginUserRequest;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/sessions")
public class UserEndPoint {
    private UserService userService;

    @Autowired
    public UserEndPoint(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> login(@Valid @RequestBody LoginUserRequest loginUserRequest, HttpServletResponse response) throws ServerException {
        String javaSessionId = UUID.randomUUID().toString();
        Cookie cookie = new Cookie("JAVASESSIONID", javaSessionId);
        response.addCookie(cookie);
        return new ResponseEntity<>(userService.loginUser(loginUserRequest, javaSessionId), HttpStatus.OK);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> logout(HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        userService.logout(cookieValue);
        return new ResponseEntity<>(new Gson().toJson(""), HttpStatus.OK);
    }
}
