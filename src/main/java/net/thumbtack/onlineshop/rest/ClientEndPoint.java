package net.thumbtack.onlineshop.rest;

import net.thumbtack.onlineshop.dto.request.EditClientProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterClientRequest;
import net.thumbtack.onlineshop.dto.response.GetInfoAboutClientsResponse;
import net.thumbtack.onlineshop.dto.response.RegisterClientResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/clients")
public class ClientEndPoint {
    private ClientService clientService;

    @Autowired
    public ClientEndPoint(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterClientResponse> registerClient(@Valid @RequestBody RegisterClientRequest registerClientRequest, HttpServletResponse response) throws ServerException {
        String javaSessionId = UUID.randomUUID().toString();
        Cookie cookie = new Cookie("JAVASESSIONID", javaSessionId);
        response.addCookie(cookie);
        return new ResponseEntity<>(clientService.registerClient(registerClientRequest, javaSessionId), HttpStatus.OK);
    }

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GetInfoAboutClientsResponse>> getInfoAboutClients(HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(clientService.getClients(cookieValue), HttpStatus.OK);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterClientResponse> editClientProfile(@Valid @RequestBody EditClientProfileRequest editClientProfileRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(clientService.editClientProfile(cookieValue, editClientProfileRequest), HttpStatus.OK);
    }
}
