package net.thumbtack.onlineshop.rest;

import net.thumbtack.onlineshop.dto.request.EditAdminProfileRequest;
import net.thumbtack.onlineshop.dto.request.RegisterAdminRequest;
import net.thumbtack.onlineshop.dto.response.RegisterAdminResponse;
import net.thumbtack.onlineshop.exception.ServerException;
import net.thumbtack.onlineshop.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/admins")
public class AdminEndPoint {
    private AdminService adminService;

    @Autowired
    public AdminEndPoint(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterAdminResponse> registerAdmin(@Valid @RequestBody RegisterAdminRequest registerAdminRequest, HttpServletResponse response) throws ServerException {
        String javaSessionId = UUID.randomUUID().toString();
        Cookie cookie = new Cookie("JAVASESSIONID", javaSessionId);
        response.addCookie(cookie);
        return new ResponseEntity<>(adminService.registerAdmin(registerAdminRequest, javaSessionId), HttpStatus.OK);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterAdminResponse> editAdminProfile(@Valid @RequestBody EditAdminProfileRequest editAdminProfileRequest, HttpServletRequest request, @CookieValue(value = "JAVASESSIONID", required = false) String cookieValue) throws ServerException {
        return new ResponseEntity<>(adminService.editAdminProfile(cookieValue, editAdminProfileRequest), HttpStatus.OK);
    }
}
