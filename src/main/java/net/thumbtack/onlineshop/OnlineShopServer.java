package net.thumbtack.onlineshop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineShopServer {
    private static final Logger logger = LoggerFactory.getLogger(OnlineShopServer.class);

    public static void main(final String[] args) {
        System.out.println("Start application");
        SpringApplication.run(OnlineShopServer.class);
        System.out.println("Stop application");
    }
}
