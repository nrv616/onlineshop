package net.thumbtack.onlineshop.model;

import java.util.Objects;

public class Basket {
    private int basketId;
    private int productId;
    private int count;

    public Basket() {
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Basket)) return false;
        Basket basket = (Basket) o;
        return getBasketId() == basket.getBasketId() &&
                getProductId() == basket.getProductId() &&
                getCount() == basket.getCount();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBasketId(), getProductId(), getCount());
    }
}
