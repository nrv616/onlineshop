package net.thumbtack.onlineshop.model;

import java.util.Objects;

public class Client extends User {
    private String email;
    private String address;
    private String phone;
    private int deposit;

    public Client(String firstName, String lastName, String patronymicName, String login, String password, String email, String address, String phone) {
        this(firstName, lastName, login, password, email, address, phone);
        this.setPatronymicName(patronymicName);
    }

    public Client(int id, String firstName, String lastName, String patronymicName, String email, String address, String phone, int deposit) {
        this(id, firstName, lastName, patronymicName, email, address, phone);
        this.deposit = deposit;
    }

    public Client(int id, String firstName, String lastName, String patronymicName, String email, String address, String phone) {
        super(id, firstName, lastName, patronymicName);
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public Client(String firstName, String lastName, String login, String password, String email, String address, String phone) {
        super(firstName, lastName, login, password);
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public Client(int id, String firstName, String lastName, String patronymicName, String password, String email, String address, String phone) {
        super(id, firstName, lastName, patronymicName, password);
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public Client() {

    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        if (!super.equals(o)) return false;
        Client client = (Client) o;
        return getId() == client.getId() &&
                Objects.equals(getEmail(), client.getEmail()) &&
                Objects.equals(getAddress(), client.getAddress()) &&
                Objects.equals(getPhone(), client.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getEmail(), getAddress(), getPhone(), getId());
    }
}
