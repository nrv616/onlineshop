package net.thumbtack.onlineshop.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Category {
    private int id;
    private String name;
    private Category parent;
    private List<Category> subCategories = new ArrayList<>();

    public Category() {

    }

    public Category(int id, String name, Category parent) {
        this.name = name;
        this.id = id;
        this.parent = parent;
    }

    public Category(int id, String name) {
        this(id, name, new Category());
    }

    public Category(int id, String name, int parentId) {
        this(id, name, new Category(parentId));
    }

    public Category(int id) {
        this.id = id;
        parent = new Category();
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }

    public Category(String name) {
        this.name = name;
        parent = new Category();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getParent() {
        return parent;
    }

    public int getParentId() {
        if (parent == null) {
            parent = new Category();
        }
        return parent.getId();
    }

    public void setParentId(int id) {
        if (parent == null) {
            parent = new Category();
        }
        parent.setId(id);
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        Category category = (Category) o;
        return Objects.equals(getName(), category.getName()) &&
                Objects.equals(getParent(), category.getParent()) &&
                Objects.equals(getSubCategories(), category.getSubCategories());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getParent(), getSubCategories());
    }
}
