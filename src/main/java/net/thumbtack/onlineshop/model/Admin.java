package net.thumbtack.onlineshop.model;

import java.util.Objects;

public class Admin extends User {
    private String position;

    public Admin(String firstName, String lastName, String patronymicName, String login, String password, String position) {
        super(firstName, lastName, patronymicName, login, password);
        this.position = position;
    }

    public Admin(int id, String firstName, String lastName, String patronymicName, String position) {
        super(id, firstName, lastName, patronymicName);
        this.position = position;
    }

    public Admin(int id, String firstName, String lastName, String patronymicName, String password, String position) {
        super(id, firstName, lastName, patronymicName, password);
        this.position = position;
    }

    public Admin() {

    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Admin admin = (Admin) o;
        return Objects.equals(position, admin.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), position);
    }
}
