package net.thumbtack.onlineshop.mappers;

import net.thumbtack.onlineshop.model.Category;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CategoryMapper {
    @Select("SELECT id, name from category where id=#{id}")
    Category getCategoryById(@Param("id") int id);

    @Select("UPDATE category SET name = #{name}, parent = #{parentId}  where id=#{id}")
    void editCategoryById(@Param("id") int id, @Param("name") String name, @Param("parentId") int parentId);

    @Select("SELECT parent from category where id=#{id}")
    Integer getParentIdById(@Param("id") int id);

    @Select({"<script>",
            "SELECT id, name, parent as parentId from category where id IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item}",
            "</foreach>",
            ") ORDER BY name",
            "</script>"})
    @Options(useGeneratedKeys = true)
    List<Category> getCategoriesById(@Param("list") List<Integer> categoryList);

    @Select("SELECT id, name, parent as parentId from category ORDER BY name")
    List<Category> getCategories();

    @Delete("DELETE from category where id=#{id}")
    void deleteCategoryById(@Param("id") int id);

    @Insert("INSERT INTO category ( id, name) VALUES "
            + "( 0, #{category.name} )")
    @Options(useGeneratedKeys = true, keyProperty = "category.id")
    int addCategory(@Param("category") Category category);

    @Insert("INSERT INTO category ( id, name, parent) VALUES "
            + "( 0, #{category.name}, #{parentId} )")
    @Options(useGeneratedKeys = true, keyProperty = "category.id")
    int addSubCategory(@Param("category") Category category, @Param("parentId") int parentId);

    @Delete("DELETE FROM category")
    void deleteAll();
}
