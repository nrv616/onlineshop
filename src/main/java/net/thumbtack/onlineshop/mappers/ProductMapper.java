package net.thumbtack.onlineshop.mappers;

import net.thumbtack.onlineshop.model.Category;
import net.thumbtack.onlineshop.model.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ProductMapper {
    @Insert("INSERT INTO product ( id, name, price, count, version) VALUES "
            + "( 0, #{product.name}, #{product.price}, #{product.count}, 0 )")
    @Options(useGeneratedKeys = true, keyProperty = "product.id")
    int addProduct(@Param("product") Product product);

    @Update("UPDATE product SET name=#{name}, count=#{count}, price=#{price}, version=#{version}+1 where id=#{id} && version = #{version}")
    @Options(useGeneratedKeys = true, keyProperty = "product.id")
    void editProduct(@Param("id") int id, @Param("name") String name, @Param("count") int count, @Param("price") int price, @Param("version") int version);

    @Select("SELECT id, name, price, count from product where id=#{id}")
    Product getProductById(@Param("id") int id);

    @Select("SELECT id, name, price, count from product ORDER BY name")
    List<Product> getProducts();

    @Select("SELECT product_id from category_product")
    List<Integer> getProductsId();

    @Select("SELECT category_id as id from category_product where product_id=#{id}")
    List<Category> getCategoriesIdOfProductById(@Param("id") int id);

    @Select("SELECT name FROM category where id in(SELECT category_id from category_product where product_id=#{id})")
    List<Category> getCategoriesNamesOfProductById(@Param("id") int id);

    @Select("SELECT id, name FROM category where id in(SELECT category_id from category_product where product_id=#{id})")
    List<Category> getCategoriesOfProductById(@Param("id") int id);

    @Select("SELECT id, name FROM category where id in(SELECT category_id from category_product) ORDER BY name")
    List<Category> getCategoriesOfProducts();

    @Select("SELECT id, name, price, count from product where id in (SELECT product_id from category_product where category_id=#{id}) ORDER BY name")
    List<Product> getProductsWithIdOfCategoryById(@Param("id") int id);

    @Select({"<script>",
            "Select id, name, price, count from product where id IN(",
            "SELECT product_id from category_product where category_id IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item}",
            "</foreach>",
            ")",
            ") ORDER BY name",
            "</script>"})
    @Options(useGeneratedKeys = true)
    List<Product> getProductsWithIdOfCategoriesById(@Param("list") List<Integer> categoryList);

    @Delete("DELETE FROM category_product where product_id = #{id}")
    void deleteCategories(@Param("id") int id);

    @Delete("DELETE FROM product where id = #{id}")
    void deleteProductById(@Param("id") int id);

    @Delete("DELETE FROM product")
    void deleteAll();

    @Insert({"<script>",
            "INSERT INTO category_product ( product_id, category_id) VALUES",
            "<foreach item='item' collection='list' separator=','>",
            "( #{productId}, #{item} )",
            "</foreach>",
            "</script>"})
    @Options(useGeneratedKeys = true)
    void addProductToCategories(@Param("list") List<Integer> categoryList, @Param("productId") int productId);

    @Update("UPDATE product SET count=#{newCount}, version=#{versionNew} where id=#{id} && version = #{version} ")
    @Options(useGeneratedKeys = true, keyProperty = "product.id")
    int setNewCount(@Param("id") int id, @Param("newCount") int newCount, @Param("version") int version, @Param("versionNew") int versionNew);

    @Select("Select version FROM product where id=#{productId}")
    int getVersionOfProduct(@Param("productId") int productId);
}
