package net.thumbtack.onlineshop.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface DepositMapper {
    @Insert("INSERT INTO deposit ( id, deposit, client_id, version) VALUES "
            + "( 0, 0, #{clientId}, 0 )")
    void createDeposit(@Param("clientId") int clientId);

    @Update("UPDATE deposit SET deposit=#{deposit}, version=#{version}+1 where client_id=#{clientId} && deposit-#{deposit}>=0 && version = #{version}")
    int putDepositPurchase(@Param("clientId") int clientId, @Param("deposit") int deposit, @Param("version") int version);

    @Update("UPDATE deposit SET deposit=#{deposit}, version=#{version}+1 where client_id=#{clientId} && version = #{version}")
    int putDeposit(@Param("clientId") int clientId, @Param("deposit") int deposit, @Param("version") int version);

    @Select("Select deposit from deposit where client_id=#{clientId}")
    int getDeposit(@Param("clientId") int clientId);

    @Select("Select version FROM deposit where client_id=#{clientId}")
    int getVersionOfDeposit(@Param("clientId") int clientId);
}
