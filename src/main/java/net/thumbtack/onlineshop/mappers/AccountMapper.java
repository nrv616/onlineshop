package net.thumbtack.onlineshop.mappers;

import net.thumbtack.onlineshop.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface AccountMapper {
    @Select("SELECT user_id from session where id =#{javaSessionId}")
    Integer getUserIdByJavaSessionId(@Param("javaSessionId") String javaSessionId);

    @Select("SELECT firstName, lastName, patronymicName, login, password, id from user where id=#{id}")
    User getUserById(@Param("id") int id);
}
