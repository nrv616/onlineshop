package net.thumbtack.onlineshop.mappers;

import net.thumbtack.onlineshop.model.Client;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ClientMapper {
    @Insert("INSERT INTO `client` ( id, email, address, phone) VALUES "
            + "( #{id}, #{client.email}, #{client.address}, #{client.phone} )")
    @Options(useGeneratedKeys = true, keyProperty = "client.id")
    Integer registerClient(@Param("client") Client client, @Param("id") Integer id);

    @Select("SELECT `client`.id as id, firstName, lastName, patronymicName, email, address, phone, deposit from `client` JOIN user on user.id = `client`.id and `client`.id=#{idUser} JOIN deposit on client_id=`client`.id")
    Client getClient(@Param("idUser") int idUser);

    @Select("SELECT `client`.id, firstName, lastName, patronymicName, email, address, phone, deposit from `client`, session, user, deposit where session.id=#{javaSessionId} AND user.id = session.user_id AND `client`.id=user.id AND client_id=`client`.id")
    Client getClientByCookie(@Param("javaSessionId") String javaSessionId);

    @Update("UPDATE `client` SET email=#{email}, address=#{address}, phone=#{phone} where id=#{id}")
    void editClientProfile(@Param("id") int id, @Param("email") String email, @Param("address") String address, @Param("phone") String phone);

    @Select("SELECT `client`.id as id, firstName, lastName, patronymicName, email, address, phone from `client` INNER JOIN user ON user.id = `client`.id")
    List<Client> getClients();


}
