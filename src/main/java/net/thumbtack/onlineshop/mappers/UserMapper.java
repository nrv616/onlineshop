package net.thumbtack.onlineshop.mappers;

import net.thumbtack.onlineshop.model.Admin;
import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.User;
import org.apache.ibatis.annotations.*;

public interface UserMapper {
    @Insert("INSERT INTO user ( firstName, lastName, patronymicName, login, password) VALUES "
            + "( #{admin.firstName}, #{admin.lastName}, #{admin.patronymicName}, #{admin.login}, #{admin.password} )")
    @Options(useGeneratedKeys = true, keyProperty = "admin.id")
    Integer register(@Param("admin") Admin admin);

    @Insert("INSERT INTO user ( firstName, lastName, patronymicName, login, password) VALUES "
            + "( #{client.firstName}, #{client.lastName}, #{client.patronymicName}, #{client.login}, #{client.password} )")
    @Options(useGeneratedKeys = true, keyProperty = "client.id")
    Integer registerCl(@Param("client") Client client);

    @Insert("INSERT INTO session ( id, user_id) VALUES "
            + "( #{javaSessionId}, #{id}) ON DUPLICATE KEY UPDATE id=#{javaSessionId}")
    void logIn(@Param("javaSessionId") String javaSessionId, @Param("id") int id);

    @Select("SELECT firstName, lastName, patronymicName, login, password, id from user where upper(login)=upper(#{login}) and password=#{password}")
    User getUser(@Param("login") String login, @Param("password") String password);

    @Select("SELECT user.id, firstName, lastName, patronymicName, login, password from user, session where session.id =#{javaSessionId} AND user.id =session.user_id ")
    User getUserByJavaSessionId(@Param("javaSessionId") String javaSessionId);

    @Update("UPDATE user SET firstName=#{client.firstName}, lastName=#{client.lastName}, patronymicName=#{client.patronymicName}, password=#{client.password} where id=#{client.id}")
    int editClientProfile(@Param("client") Client client);

    @Update("UPDATE user SET firstName=#{admin.firstName}, lastName=#{admin.lastName}, patronymicName=#{admin.patronymicName}, password=#{admin.password} where id=#{admin.id}")
    int editAdminProfile(@Param("admin") Admin admin);

    @Delete("DELETE FROM session where id =#{javaSessionId}")
    void logOut(@Param("javaSessionId") String javaSessionId);

    @Select("SELECT user_id from session where id =#{javaSessionId}")
    Integer getUserIdByJavaSessionId(@Param("javaSessionId") String javaSessionId);

    @Delete("DELETE FROM user")
    void deleteAll();
}
