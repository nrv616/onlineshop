package net.thumbtack.onlineshop.mappers;


import net.thumbtack.onlineshop.model.Client;
import net.thumbtack.onlineshop.model.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface PurchaseMapper {
    @Insert("INSERT INTO purchase ( id, clientId, productId, name, price, count) VALUES "
            + "( 0, #{clientId}, #{productId}, #{name}, #{price}, #{count} )")
    void purchase(@Param("clientId") int clientId, @Param("productId") int productId, @Param("name") String name, @Param("price") int price, @Param("count") int count);

    @Update("UPDATE basket_product SET count=#{count} where basketId=#{basketId} and productId=#{productId}")
    void purchaseBasket(@Param("basketId") int basketId, @Param("productId") int productId, @Param("count") int count);

    @Select("Select productId, name, count, price FROM purchase where clientId=#{clientId}")
    List<Product> getProductsByClientId(@Param("clientId") int clientId);

    @Select("Select productId, name, count, price FROM purchase where clientId=#{clientId} LIMIT #{offset}, #{limit}")
    List<Product> getProductsByClientIdLimit(@Param("clientId") int clientId, @Param("offset") int offset, @Param("limit") int limit);

    @Select({"<script>",
            "Select productId as id, name, price, count FROM purchase where clientId IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item}",
            "</foreach>",
            ")",
            "</script>"})
    @Options(useGeneratedKeys = true)
    List<Product> getPurchasesByClientsId(@Param("list") List<Integer> clientsId);

    @Select({"<script>",
            "Select productId as id, name, price, count FROM purchase where clientId IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item}",
            "</foreach>",
            ")",
            "LIMIT #{offset}, #{limit}",
            "</script>"})
    @Options(useGeneratedKeys = true)
    List<Product> getPurchasesByClientsIdLimit(@Param("list") List<Integer> clientsId, @Param("offset") int offset, @Param("limit") int limit);

    @Select("SELECT `client`.id as id, firstName, lastName, patronymicName, email, address, phone from `client` INNER JOIN user ON user.id = `client`.id and `client`.id in (Select clientId FROM purchase where productId=#{productId}) ORDER BY lastName")
    List<Client> getClientsByProduct(@Param("productId") int productId);

    @Select("SELECT `client`.id as id, firstName, lastName, patronymicName, email, address, phone from `client` INNER JOIN user ON user.id = `client`.id and `client`.id in (Select clientId FROM purchase where productId=#{productId}) ORDER BY lastName LIMIT #{offset}, #{limit} ")
    List<Client> getClientsByProductLimit(@Param("productId") int productId, @Param("offset") int offset, @Param("limit") int limit);

    @Select({"<script>",
            "SELECT `client`.id as id, firstName, lastName, patronymicName, email, address, phone from `client` INNER JOIN user ON user.id = `client`.id and `client`.id in (",
            "Select clientId FROM purchase where productId IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item}",
            "</foreach>",
            ")",
            ") ORDER BY lastName",
            "</script>"})
    @Options(useGeneratedKeys = true)
    List<Client> getClientsByProducts(@Param("list") List<Integer> productsId);

    @Select({"<script>",
            "SELECT `client`.id as id, firstName, lastName, patronymicName, email, address, phone from `client` INNER JOIN user ON user.id = `client`.id and `client`.id in (",
            "Select clientId FROM purchase where productId IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item}",
            "</foreach>",
            ")",
            ") ORDER BY lastName",
            "LIMIT #{offset}, #{limit}",
            "</script>"})
    @Options(useGeneratedKeys = true)
    List<Client> getClientsByProductsLimit(@Param("list") List<Integer> productsId, @Param("offset") int offset, @Param("limit") int limit);
}
