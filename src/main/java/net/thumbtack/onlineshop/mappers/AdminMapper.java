package net.thumbtack.onlineshop.mappers;

import net.thumbtack.onlineshop.model.Admin;
import org.apache.ibatis.annotations.*;

public interface AdminMapper {
    @Insert("INSERT INTO admin ( id, position) VALUES "
            + "( #{id}, #{admin.position} )")
    @Options(useGeneratedKeys = true, keyProperty = "admin.id")
    Integer registerAdmin(@Param("admin") Admin admin, @Param("id") Integer id);

    @Select("SELECT admin.id as id, firstName, lastName, patronymicName, position from admin JOIN user on user.id = admin.id and admin.id=#{idUser}")
    Admin getAdmin(@Param("idUser") int idUser);

    @Update("UPDATE admin SET position=#{position} where id=#{id}")
    void editAdminProfile(@Param("id") int id, @Param("position") String position);

    @Select("SELECT admin.id, firstName, lastName, patronymicName, position from admin, session, user where session.id=#{javaSessionId} AND user.id = session.user_id AND admin.id=user.id")
    Admin getAdminByCookie(@Param("javaSessionId") String javaSessionId);
}
