package net.thumbtack.onlineshop.mappers;

import net.thumbtack.onlineshop.model.Product;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BasketMapper {
    @Insert("INSERT INTO basket ( id, clientId) VALUES "
            + "( 0, #{clientId} )")
    void createBasket(@Param("clientId") int clientId);

    @Insert("INSERT INTO basket_product ( productId, basketId, count) VALUES "
            + "( #{productId}, #{basketId}, #{count} )")
    void addProductToBasket(@Param("basketId") int basketId, @Param("productId") int productId, @Param("count") int count);

    @Insert("UPDATE basket_product SET count=#{count} where productId=#{productId} and basketId=#{basketId}")
    void editProductFromBasket(@Param("basketId") int basketId, @Param("productId") int productId, @Param("count") int count);

    @Select("SELECT product.id, name, price, basket_product.count from product join basket_product on basketId=#{basketId} and product.id=productId")
    List<Product> getProductsInBasket(@Param("basketId") int basketId);

    @Select("SELECT id FROM basket where clientId=#{clientId}")
    int getBasketId(@Param("clientId") int clientId);

    @Select("SELECT count FROM basket_product where productId=#{productId} and basketId=#{basketId}")
    Integer getCount(@Param("basketId") int basketId, @Param("productId") int productId);

    @Delete("DELETE FROM basket_product where basketId=#{basketId} and productId=#{productId}")
    void deleteProduct(@Param("basketId") int basketId, @Param("productId") int productId);
}
